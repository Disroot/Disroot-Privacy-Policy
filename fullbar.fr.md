---
title: Déclaration de confidentialité
bgcolor: '#FFF'
fontcolor: '#1F5C60'
section_id: privacy
text_align: left
---

<br>
**v1.5 - Février 2024**

<a name="about"></a>
<br>
## A propos de ce document

Ce document a été rédigé à l'origine en anglais et est la seule version dont **Stichting Disroot.org** peut être tenu pour responsable.<br>
Toute traduction de la présente **Déclaration de confidentialité** est un effort communautaire visant à rendre l'information accessible dans d'autres langues et doit être considérée comme telle, sans autre valeur que celle d'une simple information.
<br>

<a name="definitions"></a>
<br>
## Définitions utilisées dans la présente déclaration de confidentialité

- **RGPD** : Règlement général sur la protection des données, [EU 2016/679](https://eur-lex.europa.eu/legal-content/FR/TXT/?qid=1580499932731&uri=CELEX:32016R0679)
- **Données** : Selon le **RGPD**, on entend par données toute information pouvant être utilisée pour identifier une personne, soit directement (nom réel, numéro de téléphone, adresse IP, etc.), soit indirectement (toute combinaison des éléments susmentionnés plus les empreintes digitales des appareils, les cookies, etc.) Dans le contexte spécifique de l'utilisation de notre plateforme, il s'agit des informations minimales requises pour le bon fonctionnement des services fournis par **Disroot.org** ainsi que des informations que l'utilisateur soumet facultativement sur l'un d'entre eux.
- **Services** : l'ensemble des différents logiciels, protocoles et standards utilisés pour échanger des données entre les applications web.
- **Utilisateur** ou **vous** : toute personne ou tierce partie qui accède et utilise les services fournis par **Disroot.org**.
- **Disroot, Disroot.org, nous** ou **nous** : Stichting Disroot.org
- **Plateforme** : l'ensemble des services fournis par **Disroot.org** et qui sont hébergés sur nos serveurs.
- **Identifiants Disroot** : il s'agit du nom d'utilisateur et du mot de passe créés et utilisés par l'utilisateur pour se connecter aux services que nous fournissons.
- **Services fédérés** : services qui fonctionnent sur la base de protocoles dits **fédérés** qui permettent aux utilisateurs qui se sont inscrits chez différents fournisseurs de services d'interagir entre eux. Des exemples de ces services sont **Nextcloud**, **Email**, **Akkoma** et **XMPP**.
- **Attaque par force brute** : est une attaque cryptographique qui consiste à soumettre de nombreux mots de passe ou phrases de passe, en espérant trouver éventuellement les bons.

<br>

<a name="coverage"></a>
<br>
## Les données couvertes par la présente déclaration de confidentialité

Cette **Déclaration de confidentialité** s'applique à tous les services hébergés sur **Disroot.org** et ses sous-domaines. Elle ne s'étend pas aux sites web ou services web accessibles depuis notre plateforme, y compris, mais sans s'y limiter, les services fédérés et les sites web de médias sociaux en dehors de **Disroot**. Les services fédérés sont ceux qui interagissent entre eux (échange d'informations et de services) quel que soit le fournisseur (par exemple, courrier ou réseaux sociaux ouverts). Ces services utilisent des protocoles qui partagent ou transfèrent nécessairement des données entre différents fournisseurs et, par conséquent, ces interactions ne relèvent pas du champ d'application de la présente déclaration de confidentialité.<br>
Il est important de noter que **le partage des données avec d'autres fournisseurs de services est un choix de l'utilisateur** (voir [1. Quelles données collectons-nous ?](#data_we_collect)) et est configuré par les utilisateurs dans les paramètres de leurs services, y compris la décision de savoir quoi partager et avec qui.

<br>

<a name="data_we_collect"></a>
<br>
# 1. Quelles données collectons-nous ?

Si un utilisateur choisit d'utiliser l'un des services que nous fournissons, les données suivantes seront requises et donc collectées par **Disroot.org** :

- Une adresse électronique valide : requise pour la création du compte. Cette adresse électronique est supprimée de notre base de données après que le compte a été approuvé/refusé, à moins que l'utilisateur ne choisisse, lors de la procédure d'enregistrement, de la conserver pour la procédure de réinitialisation du mot de passe.
- Un nom d'utilisateur et un mot de passe : nécessaires pour identifier le titulaire du compte et fournir les services offerts par **Disroot.org**.
- Les informations nécessaires liées au fonctionnement et à l'exploitation des services qui peuvent inclure, par exemple, l'adresse IP, le User Agent, etc. *Des informations plus détaillées à ce sujet et sur la manière dont nous les traitons peuvent être trouvées dans les  [Notices de confidentialité par service](#per_services).*
- Lorsqu'un utilisateur fait un don en ligne à **Disroot.org**, nous recueillons des données personnelles telles que, mais sans s'y limiter, le nom d'utilisateur (le cas échéant), le pays (en cas de demande de stockage supplémentaire pour des raisons fiscales), les ID de transaction ou le compte bancaire/référence. Le but pour lequel nous utilisons ces données est purement administratif (vérification des dons réguliers, gestion comptable) et est maintenu sous les mêmes mesures de sécurité décrites dans la section "[Comment stockons-nous vos données ?](#how_we_store)". Étant donné que toutes les données que nous recueillons sont préalablement traitées par un tiers chargé du traitement des paiements, tel que PayPal, Patreon ou Liberapay, en utilisant ces services ou des services similaires, l'utilisation de vos informations par ce tiers est basée sur ses conditions d'utilisation et ses politiques, et non sur les nôtres, nous vous encourageons donc à examiner attentivement ces politiques.
- Toute information supplémentaire que l'utilisateur choisit de fournir en utilisant les services fournis par nous (qu'il s'agisse de chats, de messages, de courriels, etc.). Ces informations supplémentaires sont facultatives et ne sont fournies qu'avec le consentement de l'utilisateur.

(Pour des informations plus détaillées, veuillez vous référer à la section [Notices de confidentialité par service](#per_services) ci-dessous)

<a name="what_we_do"></a>
<br>
## 1.1. Que faisons-nous de vos données ?

- Notre traitement de vos informations se limite à la fourniture du service.
- Nous conservons les journaux de votre activité pendant une période ne dépassant pas 24 heures (sauf indication contraire par service). Ces données sont utilisées pour aider à diagnostiquer les problèmes de logiciel, à maintenir la sécurité du système contre les intrusions et à surveiller la santé de la plate-forme.

(Informations détaillées dans la section [Notices de confidentialité par service](#per_services)

<a name="how_we_store"></a>
<br>
## 1.2. Comment stockons-nous vos données ?

Pour protéger vos données, nous utilisons les mesures de sécurité suivantes :

- Nous utilisons le chiffrement des disques sur tous les serveurs afin d'éviter les fuites de données en cas de vol, de confiscation ou de toute autre forme de manipulation physique des serveurs.
- Nous fournissons et exigeons un chiffrement SSL/TLS pour toutes les communications "d'utilisateur à serveur" et "de serveur à serveur" sur tous les services fournis.
- Nous utilisons des technologies de chiffrement "de bout en bout" et/ou "côté serveur" chaque fois qu'elles sont mises à disposition par des services qui permettent d'offrir une sécurité maximale aux utilisateurs.

<a name="how_we_backup"></a>
<br>
## 1.3. Comment sauvegardons-nous vos données ?

Afin de permettre une tentative de récupération en cas de perte de données, nous créons des sauvegardes sur un serveur distant appartenant à Stichting Disroot.org. Nous sauvegardons les données sur une base quotidienne et les conservons pendant une période de 4 jours.

<a name="what_we_do_not"></a>
<br>
# 2. Ce que nous ne faisons pas avec vos données

- Nous ne recueillons pas d'autres données que celles qui sont nécessaires pour vous fournir le service.
- Nous ne traitons ni n'analysons en aucune façon votre comportement ou vos caractéristiques personnelles pour créer des profils sur vous ou sur votre utilisation des services. Nous n'avons pas de publicité ni de relations commerciales avec des annonceurs.
- Nous ne vendons pas vos données à des tiers.
- Nous ne partageons pas vos données avec des tiers, sauf dans le cas de services fédérés qui nécessitent le partage de certaines données pour fonctionner (par exemple, un autre fournisseur de services de courrier électronique a besoin de connaître votre adresse électronique pour pouvoir vous envoyer des courriers électroniques).
- Nous n'exigeons aucune information supplémentaire qui ne soit pas cruciale pour le fonctionnement du service (nous ne demandons pas de numéros de téléphone, de données personnelles privées, d'adresse personnelle).
- Nous ne lisons, ni ne regardons, ni ne traitons vos données personnelles, vos courriels, vos fichiers, etc., stockés sur nos serveurs, sauf si cela s'avère nécessaire pour la mise à disposition du service, à des fins de dépannage ou en cas de suspicion de violation de nos **Conditions de service**, auquel cas nous vous demandons une autorisation préalable ou vous informons par la suite de toutes les actions prises à l'encontre du compte dans le rapport de transparence adressé au titulaire du compte.

<a name="where_store"></a>
<br>
# 3. Où les données sont-elles stockées ?

Nous stockons toutes les données dans **nos propres serveurs**, situés dans un centre de données aux **Pays-Bas**.

<a name="per_service"></a>
<br>
# 4. Notices de confidentialité détaillées par service

<a name="email"></a>
<br>
## 4.1. - **Courriel Disroot** (https://webmail.disroot.org)

- Ce service nécessite une connexion avec des identifiants **Disroot**.
- Tous les courriels, sauf s'ils sont chiffrés par l'utilisateur (avec GnuPG/PGP, par exemple) sont stockés en clair sur nos serveurs.
- Les adresses IP des utilisateurs actuellement connectés via les protocoles IMAP/POP3 sont stockées tant que l'appareil est connecté au serveur *(pour chaque appareil connecté)*.
- Les journaux du serveur, qui stockent des informations telles que, mais sans s'y limiter, votre nom d'utilisateur et votre adresse IP, les adresses de courrier électronique *de* et *à*, les adresses IP des serveurs vers lesquels les courriers électroniques arrivent ou sortent, sont stockés pendant une période de 24 heures après laquelle ils sont supprimés du serveur. Aucune sauvegarde des fichiers journaux n'est créée. Les journaux sont conservés pour prévenir les attaques de brute force sur les comptes et pour fournir un aperçu rapide lors du débogage des problèmes.
- Étant donné que le courrier électronique fonctionne selon un protocole **fédéré**, lors de l'interaction avec des adresses électroniques hébergées sur des serveurs tiers (par exemple, Gmail.com, Posteo.org), les données sont envoyées à d'autres serveurs du réseau exploités et détenus indépendamment, sur lesquels nous n'avons aucun contrôle.

<a name="cloud"></a>
<br>
## 4.2. - **Cloud Disroot** (https://cloud.disroot.org)

- Ce service nécessite une connexion avec des identifiants **Disroot**.
- Tous les fichiers envoyés sur le cloud sont chiffrés à l'aide d'une paire de clés créée à partir du mot de passe de l'utilisateur pour ajouter un niveau de sécurité supplémentaire. Notez cependant que les clés sont stockées sur le serveur, ce qui compromet le niveau de sécurité dans une certaine mesure (par exemple : si un attaquant connaît votre mot de passe et obtient la paire de clés de chiffrement, il peut déchiffrer les données). Cependant, **aucune** "clé maîtresse" n'existe dans notre configuration, ce qui signifie que les administrateurs ne peuvent déchiffrer aucun fichier stocké sur le cloud sans connaître au préalable le mot de passe de l'utilisateur.
- A l'exception des fichiers, tout le reste (calendriers, contacts, news, tâches, signets, etc.) est stocké en clair dans une base de données, sauf si une application fournit un chiffrement externe (aucun à ce jour). Il s'agit d'une limitation du logiciel que nous utilisons pour ce service (Nextcloud).
- Les journaux du serveur, qui stockent des informations telles que, mais sans s'y limiter, votre adresse IP, votre nom d'utilisateur, une application actuellement utilisée, les messages d'erreur et le User Agent, sont conservés pendant une période de 24 heures, après quoi ils sont supprimés du serveur. Aucune sauvegarde des fichiers journaux n'est créée. Les journaux sont conservés afin de prévenir les attaques de brute force sur les comptes et de fournir un aperçu rapide lors du débogage des problèmes.

<a name="chat"></a>
<br>
## 4.3. - **Chat XMPP Disroot** (https://webchat.disroot.org)

- Ce service nécessite une connexion avec des identifiants **Disroot**.
- Le fichier (votre liste de contacts XMPP) est stocké dans la base de données du serveur.
- L'historique du chat est stocké sur le serveur sous la même forme que sur le chat lui-même, ce qui signifie que le chat non chiffré est stocké en texte clair et le chat chiffré est stocké chiffré. En outre, l'historique des chats, s'il n'est pas spécifié par l'utilisateur pour chaque salle de discussion, est stocké sur le serveur pendant une période d'un mois. Vous pouvez décider de ne pas faire stocker d'historique sur le serveur par chat.
- Les journaux du serveur, qui contiennent des informations telles que, mais sans s'y limiter, votre adresse IP et votre nom d'utilisateur, sont conservés pendant une période de 24 heures, après quoi ils sont supprimés du serveur. Aucune sauvegarde des fichiers journaux n'est créée. Les journaux sont conservés pour éviter les attaques de brute-force sur les comptes et pour fournir un aperçu rapide lors du débogage des problèmes.
- Étant donné que XMPP est un protocole **fédéré**, lors de l'interaction avec des utilisateurs ou des salons de discussion hébergés sur des serveurs tiers, les données sont envoyées à d'autres serveurs du réseau, exploités et détenus indépendamment, sur lesquels nous n'avons aucun contrôle.
- Les fichiers téléchargés vers le serveur sont stockés tels quels (en clair ou chiffrés) pendant une période d'un mois.
- Si vous utilisez [Movim](https://webchat.disroot.org), tous les articles et commentaires que vous avez publiés sur nos services sont stockés sur le serveur. En utilisant le panneau de configuration de Movim, vous pouvez choisir de quitter l'instance. Dans ce cas, toutes les données liées à votre compte XMPP seront détruites.

<a name="search"></a>
<br>
## 4.4. - **Search Disroot** (https://search.disroot.org)

- Ce service ne nécessite pas de connexion ni de fournir de données personnelles.
- **Aucune donnée de connexion** (adresse IP, cookie de session, etc.) n'est stockée sur le serveur.
- Les journaux peuvent être activés occasionnellement en cas de problème. Les journaux sont alors activés pendant la durée de l'évaluation du problème et sont purgés immédiatement après.
- Les informations personnelles de nos utilisateurs ne sont jamais divulguées aux autres moteurs de recherche.

<a name="upload"></a>
<br>
## 4.5. - **Upload Disroot** (https://upload.disroot.org)

- Ce service ne nécessite pas de connexion ni de fournir de données personnelles.
- **Aucune donnée de connexion** (adresse IP, cookie de session, etc.) n'est stockée sur le serveur.
- Les journaux peuvent être activés occasionnellement en cas de problème. Les journaux sont alors activés pendant la durée de l'évaluation du problème et sont purgés immédiatement après.
- Tous les fichiers téléchargés vers le serveur sont **chiffrés de bout en bout**, ce qui signifie que personne ayant accès au serveur ne peut déchiffrer/lire les données.
- Les fichiers téléchargés vers le serveur sont effacés en fonction de la période de conservation fixée par l'utilisateur au moment du téléchargement.

<a name="pads"></a>
<br>
## 4.6. - **Pads Disroot** (https://pad.disroot.org)

- Ce service ne nécessite pas de se connecter ni de fournir de données personnelles.
- **Aucune donnée de connexion** (adresse IP, cookie de session, etc.) n'est stockée sur le serveur.
- Nous ne collectons aucune autre donnée personnelle pouvant être liée aux pads.
- Le contenu des pads est stocké sur le serveur dans la base de données tel quel (texte en clair).
- Les utilisateurs ne peuvent pas supprimer les pads, mais les pads non modifiés expirent au bout de six mois et sont ensuite retirés du serveur.

<a name="bin"></a>
<br>
## 4.7. - **Bin Disroot** (https://bin.disroot.org)

- Ce service ne nécessite pas de se connecter ni de fournir de données personnelles.
- **Aucune donnée de connexion** (adresse IP, cookie de session, etc.) n'est stockée sur le serveur.
- Les journaux peuvent être activés occasionnellement en cas de problème. Les journaux sont alors activés pendant la durée de l'évaluation du problème et sont purgés immédiatement après.
- Tous les fichiers téléchargés vers le serveur sont **chiffrés de bout en bout**, ce qui signifie que personne ayant accès au serveur ne peut déchiffrer/lire les données.
- Les fichiers téléchargés vers le serveur sont effacés en fonction de la période de conservation fixée par l'utilisateur au moment du téléchargement.
- Les commentaires et les discussions sous forme de colles sont **chiffrés de bout en bout**.

<a name="forum"></a>
<br>
## 4.8. - **Disroot Scribe** (https://scribe.disroot.org)

- Ce service nécessite la création d'un compte séparé pour interagir avec les discussions.
- Les journaux du serveur, qui contiennent des informations telles que, mais sans s'y limiter, votre adresse IP et votre nom d'utilisateur, sont conservés pendant une période de 24 heures, après quoi ils sont supprimés du serveur. Aucune sauvegarde des fichiers journaux n'est effectuée. Les journaux sont conservés pour empêcher les attaques par force brute sur les comptes et pour fournir un aperçu rapide lors du débogage des problèmes.
- Toutes les données Scribe telles que, mais sans s'y limiter, les communautés, les messages, les fichiers, etc. sont stockées sur le serveur dans la base de données en l'état (en texte clair).
- Étant donné que ActivityPub est un protocole **fédéré**, lors de l'interaction avec des utilisateurs hébergés sur des serveurs tiers, les données sont envoyées à d'autres serveurs du réseau, exploités et détenus de manière indépendante, sur lesquels nous n'avons aucun contrôle.

<a name="translate"></a>
<br>
## 4.9. - **Disroot Translate** (https://translate.disroot.org)

- Ce service ne nécessite pas de connexion.
- **Aucune donnée de connexion** (adresse IP, cookie de session, etc.) n'est stockée sur le serveur.
- Aucune donnée utilisateur n'est stockée en permanence sur le serveur.

<a name="calls"></a>
<br>
## 4.10. - **Calls Disroot** (https://calls.disroot.org)

- Ce service ne nécessite pas de connexion.
- **Aucune donnée de connexion** (adresse IP, cookie de session, etc.) n'est stockée sur le serveur.
- Aucune donnée utilisateur n'est stockée de manière permanente sur le serveur.


<a name="git"></a>
<br>
## 4.11. - **GIT Disroot** (https://git.disroot.org)

- Ce service nécessite la création d'un compte git séparé pour interagir avec les autres.
- Les journaux du serveur, qui stockent des informations telles que, mais sans s'y limiter, votre adresse IP, votre nom d'utilisateur, les messages d'erreur et le User Agent, sont conservés pendant une période de 24 heures, après quoi ils sont supprimés du serveur. Aucune sauvegarde des fichiers journaux n'est créée. Les journaux sont conservés afin de prévenir les attaques de brute-force sur les comptes et de fournir un aperçu rapide lors du débogage des problèmes.
- Toutes les données de git telles que, mais sans s'y limiter, les noms d'utilisateur, les adresses électroniques, les messages, le code, les fichiers, les versions, les demandes d'extraction, etc. sont stockées telles quelles sur le serveur dans la base de données (en texte clair).

<a name="audio"></a>
<br>
## 4.12. - **Audio Disroot** (https://mumble.disroot.org)

- Ce service nécessite de se connecter avec un nom d'utilisateur, mais il n'est pas obligatoire de l'enregistrer sur le serveur.
- **Aucune donnée de connexion** (adresse IP, cookie de session, etc.) n'est stockée sur le serveur, seul le nom d'utilisateur et son certificat associé sont sauvegardés, si l'utilisateur a décidé de s'enregistrer.

<a name="cryptpad"></a>
<br>
## 4.13. - **CryptPad Disroot** (https://cryptpad.disroot.org)

- Ce service ne nécessite pas de connexion ni de fournir de données personnelles. Toutefois, un compte peut être créé afin de sauvegarder des fichiers.
- **Aucune donnée de connexion** (adresse IP, cookie de session, etc.) n'est stockée sur le serveur.
- Tous les documents créés ou téléchargés vers le serveur sont **chiffrés de bout en bout**, ce qui signifie que personne ayant accès au serveur ne peut déchiffrer/lire les données.
- Les documents expirent après trois mois et sont ensuite supprimés du serveur, sauf si un compte a été créé, auquel cas les documents téléchargés vers le serveur sont effacés en fonction de la période de conservation fixée par l'utilisateur lors du téléchargement/de la création.

<a name="akkoma"></a>
<br>
## 4.14. - **Disroot Akkoma** (https://fe.disroot.org)

- Ce service nécessite une connexion avec des informations d'identification **Disroot**.
- Les journaux du serveur, qui contiennent des informations telles que, mais sans s'y limiter, votre adresse IP et votre nom d'utilisateur, sont conservés pendant une période de 24 heures, après quoi ils sont supprimés du serveur. Aucune sauvegarde des fichiers journaux n'est créée. Les journaux sont conservés pour empêcher les attaques par force brute sur les comptes et pour fournir un aperçu rapide lors du débogage des problèmes.
- Toutes les données d'Akkoma telles que, mais sans s'y limiter, les messages, les fichiers, etc., sont stockés sur le serveur dans la base de données en l'état (texte brut).
- Étant donné que ActivityPub est un protocole **fédéré**, lors de l'interaction avec des utilisateurs hébergés sur des serveurs tiers, les données sont envoyées à d'autres serveurs du réseau exploités et détenus de manière indépendante, sur lesquels nous n'avons aucun contrôle.

<a name="rights"></a>
<br>
# 5. Vos droits

En vertu du **RGPD**, vous disposez d'un certain nombre de droits en ce qui concerne vos données personnelles :

- **Droit d'accès** - Le droit de demander (I) des copies de vos données personnelles ou (II) l'accès aux informations que vous avez soumises et que nous détenons à tout moment.
- **Droit de rectification** - Le droit de faire rectifier vos données si elles sont inexactes ou incomplètes.*
- **Droit d'effacement** - Le droit de demander la suppression ou le retrait de vos données de nos serveurs.
- **Droit de limiter l'utilisation de vos données** - Le droit de limiter le traitement ou la manière dont nous utilisons vos données.
- **Droit à la portabilité des données** - Le droit de déplacer, copier ou transférer vos données.
- **Droit d'opposition** - Le droit de s'opposer à l'utilisation de vos données.

> *Votre* **nom d'utilisateur Disroot** *et* **adresse e-mail Disroot** *sont partie intégrante de votre compte d'utilisateur et ne peuvent être modifiés.*
> *Les noms d'utilisateur restent dans la base de données, même après une demande d'effacement, pour éviter que les anciens noms d'utilisateur ne soient réutilisés par de nouveaux utilisateurs, ce qui compromettrait la vie privée des uns et des autres et permettrait un éventuel vol d'identité. Pour cette raison, les noms d'utilisateur des comptes qui ont été effacés restent dans la base de données pour éviter d'être réutilisés. Cependant, toutes les informations personnelles liées sont supprimées de manière permanente.*

Vous avez le droit de déposer une plainte, de faire des recherches, d'exercer l'un des droits décrits ci-dessus ou de retirer votre consentement au traitement de vos données (le consentement est notre base juridique pour le traitement de vos données), en nous contactant par courrier électronique à:

- **data.protection.officer@disroot.org** - Personne responsable de la présente déclaration de confidentialité
- **info@disroot.org** - Contact pour les informations générales

Pour les besoins du **RGPD**, **Disroot.org** est le "responsable du traitement". Cela signifie que **Disroot** détermine les objectifs et la manière dont vos données sont traitées.

**Stichting Disroot.org**:
Chambre de commerce néerlandaise (KVK) numéro : 69988099

Si vous n'êtes pas satisfait de la manière dont nous traitons vos données ou si vous pensez que leur traitement n'est pas approprié, vous avez le droit d'envoyer une plainte au **Bureau des commissaires à l'information**.

**Autorité néerlandaise de protection des données (Dutch DPA)**
**Adresse postale**
Autoriteit Persoonsgegevens
PO Box 93374
2509 AJ DEN HAAG
**Téléphone:** (+31) - (0)70 - 888 85 00
**Fax:** (+31) - (0)70 - 888 85 01

<a name="access_information"></a>
<br>
## 5.1. Accès à vos informations

L'accès à vos données personnelles, aux fichiers stockés et aux autres informations que vous fournissez à l'un des services proposés par **Disroot.org** est sous votre contrôle. Cela signifie que toutes les données stockées sur nos services qui sont liées à des informations personnelles (services qui nécessitent une connexion) sont disponibles pour que vous puissiez les télécharger soit à des fins d'archivage soit pour les transférer vers un autre service compatible.

- Découvrez comment accéder à vos données personnelles et les exporter vous-même [**ici**](https://howto.disroot.org/fr/tutorials/user/gdpr)
- Modifier vos données personnelles ou supprimer votre compte [**ici**](https://user.disroot.org)

<a name="changes"></a>
<br>
# 6. Modifications apportées à la présente déclaration de confidentialité

Toute modification apportée à cette **Déclaration de confidentialité** sera rendue publique et communiquée à tous les utilisateurs via nos réseaux sociaux et notre blog. Nous vous recommandons de vérifier régulièrement si des modifications ont été apportées à la présente déclaration.

Vous pouvez suivre l'historique des modifications apportées à ce document sur notre système de contrôle de version [**ici**](https://git.disroot.org/Disroot/Disroot-Privacy-Policy/commits/branch/main) ou dans la section 6 de ce document.

#### Dernières mises à jour de cette déclaration de confidentialité :

- Février 2024 : ajout d'informations sur Translate, Movim et Scribe, suppression de Forum et Project Board, mise à jour des infos sur la période de rétention des sauvegardes.
- Avril 2023 : ajout d'informations sur les périodes de rétention pour Upload.
- Décembre 2022 : ajout d'informations sur Akkoma
- Septembre 2022 : mise à jour des informations XMPP sur la rétention des fichiers

<br>
[Retour au début](#top)
