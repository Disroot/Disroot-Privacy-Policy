---
title: "Declaración de Privacidad"
bgcolor: '#FFF'
fontcolor: '#1F5C60'
section_id: privacy
text_align: left
---

<br>
**v1.5 - Febrero, 2024**

<a name="about"></a>
<br>
## Acerca de este documento

Este documento ha sido redactado originalmente en inglés y es la única versión por la cual **Stichting Disroot.org** puede ser responsable.<br>
Cualquier traducción de esta **Declaración de Privacidad** es un esfuerzo de la comunidad para hacer accesible la información en otros idiomas y debe ser tomado como tal, sin otro valor que el de ser meramente informativo.
<br>

<a name="definitions"></a>
<br>
## Definiciones utilizadas en esta Declaración de Privacidad

- **RGPD**: Reglamento General de Protección de Datos, [EU 2016/679](https://eur-lex.europa.eu/legal-content/ES/TXT/?qid=1580499932731&uri=CELEX%3A32016R0679)
- **Datos**: De acuerdo con el **RGPD**, dato es cualquier información que pueda ser utilizada para identificar a una persona, ya sea directamente (nombre real, número de teléfono, dirección IP, etc.) o indirectamente (cualquier combinación de los mencionados anteriormente más huellas digitales de dispositivos, cookies, etc.). En el contexto específico del uso de nuestra plataforma, es la mínima información requerida para el correcto funcionamiento de los servicios provistos por **Disroot.org** así como también la información que la persona usuaria proporcione opcionalmente en cualquiera de ellos.
- **Servicios**: el conjunto de los diferentes programas, protocolos y estándares utilizados para intercambiar datos entre aplicaciones web.
- **Usuarie** o **tú**: cualquier persona o terceras partes que acceden y utilizan los servicios provistos por **Disroot.org**.
- **Disroot, Disroot.org o nosotros**: Stichting Disroot.org
- **Plataforma**: el conjunto de servicios provistos por **Disroot.org** y que están alojados en nuestros servidores.
- **Credenciales de Disroot**: son el nombre de usuarix y contraseña creados y utilizados por la persona usuaria para iniciar sesión en los servicios que proveemos.
- **Servicios federados**: servicios que operan sobre la base de los llamados **protocolos de federación** que posibilitan a las personas usuarias que se inscriben en diferentes proveedores de servicios interactuar entre ellas. Ejemplos de estos servicios son **Nextcloud**, el **Correo electrónico** y **XMPP**.
- **Ataque de fuerza bruta**: es un ataque criptográfico que consiste en enviar muchas contraseñas o frases de contraseñas, esperando encontrar finalmente las correctas.

<br>

<a name="coverage"></a>
<br>
## Los Datos cubiertos por esta Declaración de Privacidad

Esta **Declaración de Privacidad** aplica a todos los servicios alojados en **Disroot.org** y sus subdominios. No se extiende a ningún sitio o servicio web al que se pueda acceder desde nuestra plataforma, incluidos, entre otros, los servicios federados y los sitios web de redes sociales ajenos a **Disroot**. Servicios federados son aquellos que interoperan entre sí (intercambiando información y servicios) independientemente del proveedor (por ejemplo, el correo o las redes sociales abiertas). Estos servicios utilizan protocolos que necesariamente comparten o transfieren datos entre diferentes proveedores y por lo tanto dichas interacciones quedan fuera del alcance de esta Declaración de Privacidad.<br>
Es importante notar que **compartir datos con otros proveedores de servicios es una elección de la persona usuaria** (ver [1. ¿Qué datos recopilamos?](#data_we_collect)) y es configurada por lxs usuarixs en sus opciones de servicios, incluida la decisión de qué compartir y con quién.

<br>

<a name="data_we_collect"></a>
<br>
# 1. ¿Qué datos recopilamos?

Si una persona usuaria elige utilizar cualquiera de los servicios provistos por nosotros, los siguientes datos serán solicitados y por lo tanto recopilados por **Disroot.org**:

- Una dirección de correo electrónico válida: necesaria para la creación de la cuenta. Esta dirección de correo electrónico es borrada de nuestra base de datos después de que la cuenta haya sido aprobada/denegada, a menos que la persona usuaria elija durante el proceso de registro, mantenerla para el procedimiento de restablecimiento de la contraseña.
- Un nombre de usuarie y una contraseña: requeridos para identificar a la persona titular de la cuenta y proporcionarle los servicios ofrecidos por **Disroot.org**.
- Información necesaria relacionada con la operación y el funcionamiento de los servicios que puede incluir, por ejemplo, la dirección IP, el agente de usuario, etc. *Información más detallada sobre esto y cómo la gestionamos se puede encontrar en los [Avisos de privacidad por servicio](#per_services).*
- Cuando una persona usuaria realiza una donación en línea a **Disroot.org**, recopilamos datos personales tales como nombre de usuarie (si lo hubiere), país (en el caso de solicitud de almacenamiento adicional con propósitos impositivos), código de transacción o cuenta/referencia bancaria, entre otros. El propósito para el que utilizamos estos datos es meramente administrativo (verificación de donaciones regulares, gestión contable) y se mantienen bajo las mismas medidas de seguridad descritas en la sección "[¿Cómo almacenamos tus datos?](#how_we_store)". Dado que todos los datos que recopilamos son procesados previamente por un procesador de pagos de terceros como PayPal, Patreon o Liberapay, al utilizar estos servicios u otros similares, el uso que hacen de tu información se basa en sus condiciones de servicio y políticas, no en las nuestras, por lo que te recomendamos que revises dichas políticas cuidadosamente.
- Cualquier información adicional que la persona usuaria elija suministrar mientras utiliza los servicios que le proporcionamos (ya sean chats, publicaciones, correos electrónicos, etc.). Esta información adicional es opcional y con su consentimiento.

(Para información más detallada, por favor consulta la sección [Avisos de privacidad detallados por servicio](#per_services) más abajo)

<a name="what_we_do"></a>
<br>
## 1.1. ¿Qué hacemos con tus datos?

- El tratamiento de tus datos se limita a la prestación del servicio.
- Almacenamos registros de tu actividad durante un período no mayor a 24 horas (a menos que se especifique lo contrario por servicio). Estos datos se utilizan para ayudar a diagnosticar problemas de software, mantener la seguridad del sistema contra intrusiones y supervisar la salud de la plataforma.

(Información detallada en la sección [Avisos de privacidad detallados por servicio](#per_services))

<a name="how_we_store"></a>
<br>
## 1.2. ¿Cómo almacenamos tus datos?

Para proteger tus datos utilizamos las siguientes medidas de seguridad:

- Utilizamos cifrado de disco en todos los servidores para evitar fuga de datos en caso de que los servidores sean robados, confiscados o manipulados físicamente de alguna manera.
- Proporcionamos y requerimos cifrado SSL/TLS en todas las comunicaciones "usuarie-servidor" y "servidor-servidor" en todos los servicios proporcionados.
- Utilizamos tecnologías de cifrado de "extremo-a-extremo" y/o del "lado-del-servidor" siempre que estén disponibles por los servicios que lo permiten para brindar la máxima seguridad a las personas usuarias.

<a name="what_we_do_not"></a>
<br>
# 2. Lo que no hacemos con tus datos

- No recopilamos otros datos más que los necesarios para brindarte el servicio.
- En ningún caso procesamos, analizamos tu comportamiento o características personales para crear perfiles sobre ti o tu uso de los servicios. No tenemos anuncios ni relaciones comerciales con anunciantes.
- No vendemos tus datos a ningún tercero.
- No compartimos tus datos con ningún tercero, a menos que se trate de servicios federados que requieran compartir ciertos datos para poder funcionar (por ejemplo, otro proveedor de servicios de correo electrónico necesita conocer tu dirección de correo para poder entregarte mensajes).
- No solicitamos ninguna información adicional que no sea crucial para el funcionamiento del servicio (no pedimos números de teléfono, datos personales privados, dirección de domicilio).
- No leemos/miramos ni procesamos tus datos personales, correos electrónicos, archivos, etc., almacenados en nuestros servidores, a menos que sea necesario para la prestación del servicio, para la resolución de problemas o bajo sospecha de incumplimiento de nuestras **Condiciones de Servicio**, en cuyo caso te pedimos permiso previo o te informamos posteriormente de todas las acciones realizadas contra la cuenta en el informe de transparencia dirigido al titular de la misma.

<a name="where_store"></a>
<br>
# 3. ¿Dónde son almacenados los datos?

Almacenamos toda la información en **nuestros propios servidores**, ubicados en un centro de datos en **Países Bajos**.

<a name="per_service"></a>
<br>
# 4. Avisos de privacidad detallados por servicio

<a name="email"></a>
<br>
## 4.1 - **Correo de Disroot** (https://mail.disroot.org)

- Este servicio requiere iniciar sesión con credenciales de **Disroot**.
- Todos los correos electrónicos, a menos que estén cifrados por la persona usuaria (con GnuPG/PGP, por ejemplo) son almacenados sin cifrar en nuestros servidores.
- Las direcciones IP de las personas usuarias actualmente conectadas a través de los protocolos IMAP/POP3 son almacenadas por el tiempo que el dispositivo lo esté al servidor *(por cada dispositivo conectado)*.
- Los registros del servidor, que almacenan información como tu nombre de usuarie y tu dirección IP, las direcciones de correo electrónico *de* y *a*, las direcciones IP de los servidores a los que llegan o salen los correos, entre otros, son guardados durante un periodo de 24 horas, tras el cual se eliminan del servidor. No se crea ninguna copia de seguridad de los archivos de registro. Los registros se guardan para evitar ataques de fuerza bruta a las cuentas y para dar una mirada rápida a la hora de depurar problemas.
- Dado que el correo electrónico funciona con un protocolo **federado**, cuando se interactúa con direcciones de correo electrónico alojadas en servidores de terceros (p. ej., Gmail.com, Posteo.org), los datos se envían a otros servidores de la red operados y controlados de forma independiente sobre los que no tenemos control.

<a name="cloud"></a>
<br>
## 4.2 - **Nube de Disroot** (https://cloud.disroot.org)

- Este servicio requiere iniciar sesión con credenciales de **Disroot**.
- Todos los archivos enviados a la nube son cifrados con un par de claves creado a partir de la contraseña de usuarie para añadir un nivel de seguridad adicional. Ten en cuenta, sin embargo, que las claves se almacenan en el servidor, lo que compromete el nivel de seguridad hasta cierto punto (por ejemplo, si un atacante conoce tu contraseña y obtiene el par de claves de cifrado, puede descifrar los datos). Sin embargo, **no** existe una "Clave Maestra" en nuestra configuración, lo que significa que los administradores no pueden descifrar ningún archivo almacenado en la nube sin conocer previamente la contraseña de usuarie.
- Excluyendo los archivos, todo lo demás (calendarios, contactos, noticias, tareas, marcadores, etc.) se almacena sin cifrar en una base de datos, a menos que una aplicación proporcione cifrado externo (ninguna hasta ahora). Esta es una limitación del software que utilizamos para este servicio (Nextcloud).
- Los registros del servidor, que almacenan información como tu dirección IP, tu nombre de usuarie, una aplicación utilizada actualmente, los mensajes de error y el Agente de Usuario, entre otros, son guardados durante un período de 24 horas, tras el cual se eliminan del servidor. No se crea ninguna copia de seguridad de los archivos de registro. Los registros se guardan para evitar ataques de fuerza bruta a las cuentas y para dar una mirada rápida al depurar problemas.

<a name="chat"></a>
<br>
## 4.3 - **Chat XMPP de Disroot** (https://webchat.disroot.org)

- Este servicio requiere iniciar sesión con credenciales de **Disroot**.
- El listado (de tus contactos XMPP) es almacenado en la base de datos del servidor.
- El historial del chat es almacenado en el servidor de la misma manera que el chat en sí, es decir, los chats sin cifrar se guardan en texto plano y los chats cifrados se guardan cifrados. Adicionalmente, el historial del chat, si no es especificado por la persona usuaria en cada sala, se almacena por un período de un mes. Puedes elegir, en cada sala, no tener ningún historial guardado en el servidor.
- Los registros del servidor, que almacenan información como tu dirección IP y tu nombre de usuarie, son guardados durante un período de 24 horas, tras el cual se eliminan del servidor. No se crea ninguna copia de seguridad de los archivos de registro. Los registros se guardan para evitar ataques de fuerza bruta a las cuentas y para dar una mirada rápida al depurar problemas.
- Dado que XMPP es un protocolo **federado**, cuando se interactúa con usuaries o salas de chat alojadas en servidores de terceros, se envían datos a otros servidores de la red operados y controlados de forma independiente sobre los que no tenemos control.
- Los archivos subidos al servidor son almacenados tal cual (texto plano o cifrados) por un período de 1 mes.
- Si utilizas [Movim](https://webchat.disroot.org), todos los artículos y comentarios publicados por ti en nuestros servicios se almacenan en el servidor. A través del panel de configuración de Movim puedes elegir abandonar la instancia. Si lo haces, todos los datos relacionados con tu cuenta XMPP serán destruidos.

<a name="search"></a>
<br>
## 4.4 - **Búsqueda de Disroot** (https://search.disroot.org)

- Este servicio no requiere iniciar sesión o brindar ningún dato personal.
- **Ningún registro de datos** (dirección IP, cookies de sesión, etc.) es almacenado en el servidor.
- Los registros pueden habilitarse ocasionalmente en caso de resolución de problemas. Estos son habilitados mientras dura la evaluación del problema y purgados inmediatamente después.
- La información personal de las personas usuarias de nuestros servicios nunca es filtrada a los otros motores de búsqueda.

<a name="upload"></a>
<br>
## 4.5 - **Subida de Disroot** (https://upload.disroot.org)

- Este servicio no requiere iniciar sesión o brindar ningún dato personal.
- **Ningún registro de datos** (dirección IP, cookies de sesión, etc.) es almacenado en el servidor.
- Los registros pueden habilitarse ocasionalmente en caso de resolución de problemas. Estos son habilitados mientras dura la evaluación del problema y purgados inmediatamente después.
- Todos los archivos subidos al servidor están **cifrados de extremo-a-extremo** lo que significa que nadie con acceso al servidor puede descifrar/leer los datos.
- Los archivos subidos al servidor son eliminados de acuerdo al período de retención establecido por la persona usuaria al momento de subirlos.

<a name="pads"></a>
<br>
## 4.6 - **Blocs de Disroot** (https://pad.disroot.org)

- Este servicio no requiere iniciar sesión o brindar ningún dato personal.
- **Ningún registro de datos** (dirección IP, cookies de sesión, etc.) es almacenado en el servidor.
- No recopilamos ningún dato personal que pueda ser vinculado con los blocs.
- El contenido del bloc es almacenado en la base de datos del servidor tal cual (texto plano).
- Los blocs sin actividad expiran luego de seis meses y son eliminados del servidor.

<a name="bin"></a>
<br>
## 4.7 - **Bin de Disroot** (https://bin.disroot.org)

- Este servicio no requiere iniciar sesión o brindar ningún dato personal.
- **Ningún registro de datos** (dirección IP, cookies de sesión, etc.) es almacenado en el servidor.
- Los registros pueden habilitarse ocasionalmente en caso de resolución de problemas. Estos son habilitados mientras dura la evaluación del problema y purgados inmediatamente después.
- Todos los archivos subidos al servidor están **cifrados de extremo-a-extremo** lo que significa que nadie con acceso al servidor puede descifrar/leer los datos.
- Los archivos subidos al servidor son eliminados de acuerdo al período de retención establecido por la persona usuaria al momento de subirlos.
- Los comentarios y discusiones en los "paste" están **cifrados de extremo-a-extremo**.

<a name="scribe"></a>
<br>
## 4.8. - **Disroot Scribe** (https://scribe.disroot.org)

- Este servicio requiere crear una cuenta separada para interactuar en las discusiones.
- Los registros del servidor, que almacenan información como tu dirección IP y tu nombre de usuarie, son guardados durante un período de 24 horas, tras el cual se eliminan del servidor. No se crea ninguna copia de seguridad de los archivos de registro. Los registros se guardan para evitar ataques de fuerza bruta a las cuentas y para dar una mirada rápida al depurar problemas.
- Todos los datos de Scribe como, por ejemplo, comunidades, mensajes, archivos, etc., se almacenan en el servidor tal cual (texto plano).
- Dado que ActivityPub es un protocolo **federado**, al interactuar con usuaries alojados en servidores de terceros, se envían datos a otros servidores de la red operados y controlados de forma independiente sobre los que no tenemos control.

<a name="translate"></a>
<br>
## 4.9. - **Translate de Disroot** (https://translate.disroot.org)

- Este servicio no requiere iniciar sesión.
- **Ningún registro de datos** (dirección IP, cookies de sesión, etc.) es almacenado en el servidor.
- Ningún dato de la persona usuaria es almacenado de forma permanente en el servidor.

<a name="calls"></a>
<br>
## 4.10 - **Llamadas de Disroot** (https://calls.disroot.org)

- Este servicio no requiere iniciar sesión.
- **Ningún registro de datos** (dirección IP, cookies de sesión, etc.) es almacenado en el servidor.
- Ningún dato de la persona usuaria es almacenado de forma permanente en el servidor.


<a name="git"></a>
<br>
## 4.11 - **GIT de Disroot** (https://git.disroot.org)

- Este servicio requiere la creación de una cuenta de Git separada para interactuar con otras personas.
- Los registros del servidor, que almacenan información como tu dirección IP, tu nombre de usuarie, mensajes de error y Agente de Usuarie, entre otros, son guardados durante un período de 24 horas, tras el cual se eliminan del servidor. No se crea ninguna copia de seguridad de los archivos de registro. Los registros se guardan para evitar ataques de fuerza bruta a las cuentas y para dar una mirada rápida al depurar problemas.
- Todos los datos de Git tales como nombres de usuaries, direcciones de correo, mensajes, código, archivos, versiones, pedidos de integración (pull requests), etc., son almacenados en la base de datos del servidor tal cual (texto plano).

<a name="audio"></a>
<br>
## 4.12 - **Audio de Disroot** (https://mumble.disroot.org)

- Este servicio requiere un nombre de usuarie para conectarse, pero no es obligatorio registrarlo en el servidor.
- **Ningún registro de datos** (dirección IP, cookies de sesión, etc.) es almacenado en el servidor, solo el nombre de usuarie y su certificado asociado son guardados si la persona usuaria decidió registrarse.

<a name="cryptpad"></a>
<br>
## 4.13 - **CryptPad de Disroot** (https://cryptpad.disroot.org)

- Este servicio no requiere iniciar sesión o brindar ningún dato personal. Sin embargo, se puede crear una cuenta para poder guardar archivos.
- **Ningún registro de datos** (dirección IP, cookies de sesión, etc.) es almacenado en el servidor.
- Todos los documentos creados o subidos al servidor están **cifrados de extremo-a-extremo** lo que significa que nadie con acceso al servidor puede descifrar/leer los datos.
- Los documentos expiran después de tres meses y luego son eliminados del servidor, excepto si fue creada una cuenta, en cuyo caso los documentos subidos al servidor son eliminados de acuerdo con el período de retención establecido por la persona usuaria al momento de subirlos/crearlos.

<a name="akkoma"></a>
<br>
## 4.14. - **Akkoma de Disroot** (https://fe.disroot.org)

- Este servicio requiere iniciar sesión con las credenciales de **Disroot**.
- Los registros del servidor, que almacenan información como tu dirección IP, tu nombre de usuarie, mensajes de error y Agente de Usuarie, entre otros, son guardados durante un período de 24 horas, tras el cual se eliminan del servidor. No se crea ninguna copia de seguridad de los archivos de registro. Los registros se guardan para evitar ataques de fuerza bruta a las cuentas y para dar una mirada rápida al depurar problemas.
- Todos los datos de Akkoma como, por ejemplo, mensajes, archivos, etc., son almacenados en el servidor tal cual (texto plano).
- Dado que ActivityPub es un protocolo **federado**, al interactuar con usuaries alojados en servidores de terceros, se envían datos a otros servidores de la red operados y controlados de forma independiente sobre los que no tenemos control.

<a name="rights"></a>
<br>
# 5. Tus derechos

Bajo el **RGPD** tienes una serie de derechos con respecto a tus datos personales:

- **Derecho al acceso**: El derecho a solicitar en cualquier momento (I) copias de tus datos personales o (II) acceso a la información que suministraste y que nosotros guardamos.
- **Derecho a la rectificación**: El derecho a que tus datos sean rectificados si son inexactos o incompletos.
- **Derecho a la supresión**: El derecho a solicitar la eliminación de tus datos de nuestros servidores.
- **Derecho a la limitación del tratamiento de tus datos**: El derecho a restringir el procesamiento o limitar la forma en que usamos tus datos.
- **Derecho a la portabilidad de los datos**: Derecho a mover, copiar o transferir tus datos.
- **Derecho de oposición**: El derecho a oponerte al uso que hacemos de tus datos.

> *Tu* **nombre de usuarie de Disroot** *y tu* **dirección de correo electrónico de Disroot** *son partes integrales de tu cuenta y no pueden ser modificados.*
> *Los nombres de usuaries permanecen en la base de datos, incluso después de solicitar su eliminación, para prevenir que sean reutilizados por nuevas personas usuarias, comprometiendo la privacidad de ambos y posibilitando potenciales robos de identidad. Por esta razón es que se conservan en la base de datos los nombres de usuaries que han sido eliminados: para evitar que sean reutilizados. Sin embargo, toda la información personal vinculada es borrada permanentemente.*

Tienes derecho a presentar una queja, realizar consultas, ejercer cualquiera de los derechos descritos anteriormente o retirar tu consentimiento para el tratamiento de tus datos (donde el consentimiento es nuestra base legal para dicho tratamiento), poniéndote en contacto con nosotros por correo electrónico a:

- **data.protection.officer@disroot.org**: La persona responsable de esta Declaración de Privacidad.
- **info@disroot.org**: Contacto para información general.

A los efectos del **RGPD**, **Disroot.org** es el "controlador de datos". Esto significa que **Disroot** determina los propósitos y la manera por los cuales tus datos son procesados.

**Stichting Disroot.org**:
Cámara Neerlandesa de Comercio (KVK) número: 69988099

Si no te satisface con la forma en que tratamos tus datos, o piensas que su tratamiento no es adecuado, tienes derecho a enviar un reclamo a la **Oficina del Comisario de Información**.

**Autoridad Neerlandesa de Protección de Datos (Dutch DPA)**
**Dirección postal**
Autoriteit Persoonsgegevens
PO Box 93374
2509 AJ DEN HAAG
**Teléfono:** (+31) - (0)70 - 888 85 00
**Fax:** (+31) - (0)70 - 888 85 01

<a name="access_information"></a>
<br>
## 5.1. Acceso a tu información

El acceso a tus datos personales, archivos almacenados y otra información que brindes en cualquiera de los servicios ofrecidos por **Disroot.org** está bajo tu control. Esto significa que todos los datos almacenados en nuestros servicios que están vinculados a la información personal (servicios que requieren iniciar sesión) están disponibles para que puedas descargarlos ya sea con el fin de archivarlos o transferirlos a otro servicio compatible.

- Conoce cómo acceder y exportar tus datos personales [**aquí**](https://howto.disroot.org/es/tutorials/user/gdpr)
- Modifica tus datos personales o borra tu cuenta [**aquí**](https://user.disroot.org)

<a name="changes"></a>
<br>
# 6. Cambios en esta Declaración de Privacidad

Todos y cada uno de los cambios a esta **Declaración de Privacidad** estará disponible públicamente y será comunicado a todas las personas usuarias a través de nuestras redes sociales y publicaciones en el blog. Recomendamos que consultes regularmente por cualquier cambio en esta Declaración.

Puedes seguir el historial de cambios en este documento en nuestro sistema de control de versiones [**aquí**](https://git.disroot.org/Disroot/Disroot-Privacy-Policy/commits/branch/main) o el [**registro de cambios**](https://git.disroot.org/Disroot/Disroot-Privacy-Policy/src/branch/main/changelog) para información más detallada.


#### Última actualización en esta Declaración de Privacidad:

- Febrero, 2024: agregada información sobre Translate, Movim y Scribe, removidos el Foro y el Tablero de Proyectos.
- Abril, 2023: agregada información de Subida sobre los tiempos de retención.
- Diciembre, 2022: agregada información sobre Akkoma.
- Septiembre, 2022: actualizada la información de XMPP sobre la retención de archivos.

<br>
[Volver](#top)
