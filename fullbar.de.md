---
title: Datenschutzerklärung
bgcolor: '#FFF'
fontcolor: '#1F5C60'
section_id: privacy
text_align: left
---

<br>
**v1.5 - Februar 2024**

<a name="about"></a>
<br>
## Über dieses Dokument

Dieses Dokument wurde ursprünglich auf Englisch verfasst. DIe englische Version ist die einzige Version des Dokuments, für die **Stichting Disroot.org** haftbar gemacht werden kann.<br>
Jegliche Übersetzung dieser **Datenschutzerklärung** ist eine Bemühung der Gemeinschaft, um die Informationen in anderen Sprachen zugänglich zu machen, und sollte als solche verstanden werden. Die Übersetzung hat einen rein informativen Charakter.
<br>

<a name="definitions"></a>
<br>
## In dieser Datenschutzerklärung verwendete Definitionen

- **GDPR**: Allgemeine Datenschutzverordnung, [EU 2016/679](https://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1580499932731&uri=CELEX:32016R0679)
- **Daten**: Nach der **DSGVO** sind Daten alle Informationen, die zur Identifizierung einer Person verwendet werden können, entweder direkt (richtiger Name, Telefonnummer, IP-Adresse usw.) oder indirekt (jede Kombination der vorgenannten Daten sowie Geräte-Fingerabdrücke, Cookies usw.). Im spezifischen Kontext der Nutzung unserer Plattform handelt es sich um die Mindestinformationen, die für das ordnungsgemäße Funktionieren der von **Disroot.org** bereitgestellten Dienste erforderlich sind, sowie um die Informationen, die der Nutzer optional für einen dieser Dienste angibt.
- **Dienste**: die verschiedenen Softwares, Protokolle und Standards, die zum Austausch von Daten zwischen Webanwendungen verwendet werden.
- **Benutzer** oder **Sie**: jede Person oder jeder Dritte, die oder der auf die von **Disroot.org** bereitgestellten Dienste zugreift und sie nutzt.
- **Disroot, Disroot.org, wir** oder **uns**: Die Disroot.org-Stiftung
- **Plattform**: die von **Disroot.org** angebotenen Dienste, die auf unseren Servern gehostet werden.
- **Disroot-Anmeldedaten**: der Benutzername und das Passwort, die vom Benutzer erstellt und verwendet werden, um sich bei den von uns angebotenen Diensten anzumelden.
- **Föderierte Dienste**: Dienste, die auf der Grundlage von so genannten **Föderationsprotokollen** funktionieren, die es Nutzern, die sich bei verschiedenen Dienstanbietern angemeldet haben, ermöglichen, miteinander zu interagieren. Beispiele für diese Dienste sind **Nextcloud**, **E-Mail**, **Akkoma** und **XMPP**.
- **Brute-Force-Angriff**: ist ein kryptografischer Angriff, der darin besteht, viele Passwörter oder Passphrasen einzureichen und zu testen, in der Hoffnung, schließlich die richtigen zu finden.

<br>

<a name="coverage"></a>
<br>
## Von dieser Datenschutzerklärung erfassten Daten

Diese **Datenschutzerklärung** gilt für alle Dienste, die auf **Disroot.org** und seinen Subdomains gehostet werden. Sie erstreckt sich nicht auf andere Websites oder Webdienste, auf die von unserer Plattform aus zugegriffen werden kann, einschließlich, aber nicht beschränkt auf föderierte Dienste und Social-Media-Websites außerhalb von **Disroot**. Föderierte Dienste sind Dienste, die miteinander interagieren (Informationen und Dienste austauschen), unabhängig vom Anbieter (z. B. E-Mail oder offene soziale Netzwerke). Diese Dienste verwenden Protokolle, die notwendigerweise Daten zwischen verschiedenen Anbietern austauschen oder übertragen, und daher fallen solche Interaktionen nicht in den Geltungsbereich dieser Datenschutzerklärung.<br>
Es ist wichtig, darauf hinzuweisen, dass die **gemeinsame Nutzung von Daten mit anderen Dienstanbietern eine Entscheidung des Nutzers ist** (siehe [1. Welche Daten sammeln wir?](#data_we_collect)) und von den Nutzern in ihren Diensteinstellungen konfiguriert wird, einschließlich der Entscheidung, was und mit wem geteilt werden soll.
<br>
<a name="data_we_collect"></a>
<br>
# 1. Welche Daten erheben wir?

Wenn ein Nutzer sich entscheidet, einen der von uns angebotenen Dienste zu nutzen, werden die folgenden Daten benötigt und daher von **Disroot.org** gesammelt:

- Eine gültige E-Mail-Adresse: wird für die Kontoerstellung benötigt. Diese E-Mail-Adresse wird aus unserer Datenbank gelöscht, nachdem das Konto genehmigt/abgelehnt wurde, es sei denn, der Benutzer entscheidet sich während des Registrierungsprozesses dafür, sie für den Prozess der Passwortrücksetzung zu behalten.
- Ein Benutzername und ein Passwort: sind erforderlich, um den Kontoinhaber zu identifizieren und die von **Disroot.org** angebotenen Dienste bereitzustellen.
- Notwendige Informationen im Zusammenhang mit dem Betrieb und dem Funktionieren der Dienste, wie z.B. IP-Adresse, User Agent, etc. *Ausführlichere Informationen dazu und wie wir damit umgehen, finden Sie in den [Datenschutzhinweisen der einzelnen Dienste](#per_services)*.
- Wenn ein Nutzer eine Online-Spende an **Disroot.org** tätigt, erheben wir personenbezogene Daten wie z. B. den Benutzernamen (falls vorhanden), das Land (falls eine zusätzliche Speicherung zu Steuerzwecken gewünscht wird), die Transaktions-IDs oder die Bankverbindung. Der Zweck, für den wir diese Daten verwenden, ist rein administrativer Natur (Überprüfung regelmäßiger Spenden, Verwaltung der Buchhaltung) und wird unter denselben Sicherheitsmaßnahmen aufrechterhalten, die im Abschnitt "[Wie speichern wir Ihre Daten?](#how_we_store)" beschrieben sind. Da alle von uns gesammelten Daten zuvor von einem Drittanbieter wie PayPal, Patreon oder Liberapay verarbeitet werden, unterliegt die Verwendung Ihrer Daten durch diese oder ähnliche Dienste deren Nutzungsbedingungen und Richtlinien, nicht unseren, daher empfehlen wir Ihnen, diese Richtlinien sorgfältig zu lesen.
- Alle zusätzlichen Informationen, die der Nutzer während der Nutzung der von uns angebotenen Dienste angibt (Chats, Beiträge, E-Mails usw.). Diese zusätzlichen Informationen sind fakultativ und bedürfen der Zustimmung des Nutzers. Ausführlichere Informationen finden Sie im Abschnitt ["Detaillierte Datenschutzhinweise für die einzelnen Dienste"](#per_services) weiter unten)

<a name="what_we_do"></a>
<br>
## 1.1. Was machen wir mit Ihren Daten?

- Die Verarbeitung Ihrer Daten ist auf die Bereitstellung des Dienstes beschränkt.
- Wir speichern Protokolle Ihrer Aktivitäten für einen Zeitraum von höchstens 24 Stunden (sofern für den jeweiligen Dienst nicht anders angegeben). Diese Daten werden verwendet, um Softwareprobleme zu diagnostizieren, die Sicherheit des Systems gegen Eindringlinge zu gewährleisten und den Zustand der Plattform zu überwachen.

(Ausführlichere Informationen finden Sie im Abschnitt ["Detaillierte Datenschutzhinweise für die einzelnen Dienste"](#per_services) weiter unten)

<a name="how_we_store"></a>
<br>
## 1.2. Wie speichern wir Ihre Daten?

Zum Schutz Ihrer Daten setzen wir die folgenden Sicherheitsmaßnahmen ein:

- Wir verwenden eine Festplattenverschlüsselung auf allen Servern, um Datenverluste zu verhindern, falls die Server gestohlen, beschlagnahmt oder in irgendeiner Weise physisch manipuliert werden.
- Wir bieten und verlangen SSL/TLS-Verschlüsselung für die gesamte "Benutzer-zu-Server"- und "Server-zu-Server"-Kommunikation für alle angebotenen Dienste.
- Wir verwenden "Ende-zu-Ende"- und/oder "Server-seitige" Verschlüsselungstechnologien, wann immer diese von Diensten zur Verfügung gestellt werden, die es erlauben, maximale Sicherheit für die Nutzer zu bieten.

<a name="how_we_backup"></a>
<br>


## 1.3. Wie sichern wir Ihre Daten?

Um im Falle eines Datenverlustes Wiederherstellungsversuche zu ermöglichen, erstellen wir Backups auf einem Remote-Server, der der Disroot Stiftung gehört. Die Daten werden täglich gesichert und für 4 Tage gespeichert.

<a name="what_we_do_not"></a>
<br>
# 2. Was wir mit Ihren Daten nicht tun

- Wir sammeln keine anderen Daten als die, die für die Bereitstellung des Dienstes erforderlich sind.
- Wir verarbeiten oder analysieren in keiner Weise Ihr Verhalten oder Ihre persönlichen Merkmale, um Profile über Sie oder Ihre Nutzung der Dienste zu erstellen. Wir haben keine Werbung oder Geschäftsbeziehungen mit Werbekunden.
- Wir verkaufen Ihre Daten nicht an Dritte.
- Wir geben Ihre Daten nicht an Dritte weiter, es sei denn, es handelt sich um föderierte Dienste, bei denen die Weitergabe bestimmter Daten für den Betrieb erforderlich ist (z. B. muss ein anderer E-Mail-Dienstanbieter Ihre E-Mail-Adresse kennen, um E-Mails zustellen zu können).
- Wir verlangen keine zusätzlichen Informationen, die für den Betrieb des Dienstes nicht unerlässlich sind (wir fragen nicht nach Telefonnummern, privaten persönlichen Daten, Wohnanschrift).
- Wir lesen/sehen Ihre persönlichen Daten, E-Mails, Dateien usw., die auf unseren Servern gespeichert sind, nicht ein und verarbeiten sie auch nicht, es sei denn, sie werden für die Erbringung des Dienstes oder zur Fehlerbehebung benötigt oder es besteht der Verdacht, dass Sie gegen unsere **Nutzungsbedingungen** verstoßen.

<a name="where_store"></a>
<br>
# 3. Wo werden die Daten gespeichert?

Wir speichern alle Daten auf **unseren eigenen Servern**, in einem Rechenzentrum in den **Niederlanden**.

<a name="per_service"></a>
<br>
# 4. Detaillierte Datenschutzhinweise für jeden der Dienste

<a name="email"></a>
<br>
## 4.1. - **Disroot Email** (https://webmail.disroot.org)

- Für diesen Dienst ist eine Anmeldung mit **Disroot**-Zugangsdaten erforderlich.
- Alle E-Mails werden unverschlüsselt auf unseren Servern gespeichert, es sei denn, sie werden vom Benutzer verschlüsselt (z. B. mit GnuPG/PGP).
- IP-Adressen von derzeit über IMAP/POP3-Protokolle angemeldeten Benutzern werden gespeichert, solange das Gerät auf dem Server angemeldet ist *(für jedes angemeldete Gerät).*
- Serverprotokolle, die Informationen wie z. B. Ihren Benutzernamen und Ihre IP-Adresse, *Von*- und *An*- E-Mail-Adressen sowie IP-Adressen von Servern, auf denen die E-Mails ein- oder ausgehen, speichern, werden für einen Zeitraum von 24 Stunden gespeichert und danach vom Server gelöscht. Es wird kein Backup der Protokolldateien erstellt. Die Protokolle werden aufbewahrt, um Brute-Force-Angriffe auf Konten zu verhindern und um bei der Fehlersuche einen schnellen Einblick zu erhalten.
- Da E-Mail über ein **föderiertes Protokoll** funktioniert, werden bei der Interaktion mit E-Mail-Adressen, die auf Servern von Drittanbietern gehostet werden (z.B. Gmail.com, Posteo.org), Daten an andere unabhängig betriebene und eigene Server im Netzwerk gesendet, über die wir keine Kontrolle haben.

<a name="cloud"></a>
<br>
## 4.2. - **Disroot Cloud** (https://cloud.disroot.org)

- Für diesen Dienst ist eine Anmeldung mit **Disroot**-Zugangsdaten erforderlich.
- Alle an die Cloud gesendeten Dateien werden mit einem Schlüsselpaar verschlüsselt, das auf der Grundlage des Benutzerpassworts erstellt wird, um eine zusätzliche Sicherheitsebene zu schaffen. Beachten Sie jedoch, dass die Schlüssel auf dem Server gespeichert werden, was das Sicherheitsniveau bis zu einem gewissen Grad beeinträchtigt (z. B. kann ein Angreifer, der Ihr Passwort kennt und das Schlüsselpaar erhält, die Daten entschlüsseln). In unserem System gibt es jedoch **keinen** "Hauptschlüssel", was bedeutet, dass die Administratoren keine in der Cloud gespeicherten Dateien entschlüsseln können, ohne zuvor das Passwort des Benutzers zu kennen.
- Mit Ausnahme der Dateien wird alles andere (Kalender, Kontakte, Nachrichten, Aufgaben, Lesezeichen usw.) unverschlüsselt in einer Datenbank gespeichert, es sei denn, eine Anwendung bietet eine externe Verschlüsselung an (bisher keine). Dies ist eine Einschränkung der Software, die wir für diesen Dienst verwenden (Nextcloud).
- Die Serverprotokolle, die Informationen wie z. B. Ihre IP-Adresse, Ihren Benutzernamen, eine derzeit verwendete Anwendung, Fehlermeldungen und den Benutzeragenten speichern, werden für einen Zeitraum von 24 Stunden gespeichert und danach vom Server gelöscht. Es wird kein Backup der Protokolldateien erstellt. Die Protokolle werden aufbewahrt, um Brute-Force-Angriffe auf Konten zu verhindern und um bei der Fehlersuche einen schnellen Einblick zu erhalten.

<a name="chat"></a>
<br>
## 4.3. - **Disroot-XMPP-Chat** (https://webchat.disroot.org)

- Für diesen Dienst ist eine Anmeldung mit **Disroot**-Zugangsdaten erforderlich.
- Dienste-Listen (Ihre XMPP-Kontaktliste) wird in der Datenbank des Servers gespeichert.
- Der Chatverlauf wird auf dem Server in der gleichen Form wie der Chat selbst gespeichert, d. h. unverschlüsselter Chat wird im Klartext und verschlüsselter Chat wird verschlüsselt gespeichert. Außerdem wird der Chatverlauf, wenn er nicht vom Benutzer für den jeweiligen Chatraum festgelegt wurde, 1 Monat lang auf dem Server gespeichert. Sie können für jeden Chat gesondert festlegen, dass kein Verlauf auf dem Server gespeichert wird.
- Serverprotokolle, die Informationen wie z.B. Ihre IP-Adresse und Ihren Benutzernamen speichern, werden für einen Zeitraum von 24 Stunden gespeichert und danach vom Server gelöscht. Es wird keine Sicherungskopie der Protokolldateien erstellt. Die Protokolle werden aufbewahrt, um Brute-Force-Angriffe auf Konten zu verhindern und um bei der Fehlersuche einen schnellen Einblick zu erhalten.
- Da XMPP über ein **föderiertes Protokoll** funktioniert, werden bei der Interaktion mit Benutzern oder Chat-Räumen, die auf Servern von Drittanbietern gehostet werden, Daten an andere, unabhängig betriebene und im Besitz von Drittanbietern befindliche Server im Netzwerk gesendet, über die wir keine Kontrolle haben.
- Auf den Server hochgeladene Dateien werden unverändert (im Klartext oder verschlüsselt) 1 Monat gespeichert.
- Wenn Sie [Movim] (https://webchat.disroot.org) verwenden, werden alle von Ihnen auf unseren Diensten veröffentlichten Artikel und Kommentare auf dem Server gespeichert. Über das Movim-Konfigurationspanel können Sie sich entscheiden, die Instanz zu verlassen. Wenn Sie das tun, werden alle Daten Ihres XMPP-Kontos zerstört.



<a name="search"></a>
<br>
## 4.4. - **Disroot-Suche** (https://search.disroot.org)

- Für diesen Dienst ist keine Anmeldung oder Angabe persönlicher Daten erforderlich.
- Es werden **keine Log-Daten** (IP-Adresse, Session-Cookie, etc.) auf dem Server gespeichert.
- Eine Protokollierung kann gelegentlich für die Fehlersuche aktiviert werden. Die Protokolle werden dann für die Dauer der Problembewertung aktiviert und unmittelbar danach gelöscht.
- Die persönlichen Daten unserer Nutzer werden niemals an andere Suchmaschinen weitergegeben.

<a name="upload"></a>
<br>
## 4.5. - **Disroot-Upload** (https://upload.disroot.org)

- Für diesen Dienst ist keine Anmeldung oder Angabe persönlicher Daten erforderlich.
- Es werden **keine Log-Daten** (IP-Adresse, Session-Cookie, etc.) auf dem Server gespeichert.
- Eine Protokollierung kann gelegentlich für die Fehlersuche aktiviert werden. Die Protokolle werden dann für die Dauer der Problembewertung aktiviert und unmittelbar danach gelöscht.
- Alle auf den Server hochgeladenen Dateien werden **Ende-zu-Ende verschlüsselt**, d. h. niemand, der Zugang zum Server hat, kann die Daten entschlüsseln/lesen.
- Die auf den Server hochgeladenen Dateien werden entsprechend der vom Benutzer beim Hochladen festgelegten Aufbewahrungsfrist gelöscht.
Es gibt jedoch maximale Verfügbarkeitsdauer, die automatisch in Abhängigkeit von der Dateigröße festgelegt werden (je größer die Datei ist, desto kürzer wird sie auf dem Server aufbewahrt).

<a name="pads"></a>
<br>
## 4.6. - **Disroot-Pads** (https://pad.disroot.org)

- Für diesen Dienst ist keine Anmeldung oder Angabe persönlicher Daten erforderlich.
- Es werden **keine Log-Daten** (IP-Adresse, Session-Cookie, etc.) auf dem Server gespeichert.
- Wir sammeln keine anderen persönlichen Daten, die mit den Pads in Verbindung gebracht werden können.
- Der Inhalt der Pads wird auf dem Server in der Datenbank in unveränderter Form (Klartext) gespeichert.
- Als Benutzer kann man Pads nicht aktiv löschen, aber unangetastete Pads verfallen nach sechs Monaten und werden dann vom Server entfernt.

<a name="bin"></a>
<br>
## 4.7. - **Disroot-PrivateBin** (https://bin.disroot.org)

- Für diesen Dienst ist keine Anmeldung oder Angabe von persönlichen Daten erforderlich.
- Es werden **keine Log-Daten** (IP-Adresse, Session-Cookie, etc.) auf dem Server gespeichert.
- Eine Protokollierung kann gelegentlich für die Fehlersuche aktiviert werden. Die Protokolle werden dann für die Dauer der Problembewertung aktiviert und unmittelbar danach gelöscht.
- Alle auf den Server hochgeladenen Dateien werden **Ende-zu-Ende verschlüsselt**, d. h. niemand, der Zugang zum Server hat, kann die Daten entschlüsseln/lesen.
- Auf den Server hochgeladene Dateien werden entsprechend der vom Benutzer beim Hochladen festgelegten Aufbewahrungsfrist gelöscht.
- Kommentare und Diskussionen unter Pasten sind **Ende-zu-Ende verschlüsselt**.

<a name="scribe"></a>
<br>
## 4.8. - **Disroot-Scribe** (https://scribe.disroot.org)

- Dieser Dienst erfordert die Erstellung eines separaten Kontos oder die Verwendung von **Disroot**-Anmeldeinformationen, um an Diskussionen teilzunehmen.
- Serverlogs, die Informationen wie z.B. Ihre IP-Adresse und Ihren Benutzernamen speichern, werden für einen Zeitraum von 24 Stunden aufbewahrt und danach vom Server gelöscht. Es wird keine Sicherungskopie der Protokolldateien erstellt. Die Protokolle werden aufbewahrt, um Brute-Force-Attacken zu verhindern und um bei der Fehlersuche zu helfen.
- Alle Scribe-Daten, wie z. B. Communities, Nachrichten, Dateien usw., werden auf dem Server im Klartext in der Datenbank gespeichert.
- Da es sich bei ActivityPub um ein **föderiertes** Protokoll handelt, werden bei der Interaktion mit Nutzern, die auf Servern von Drittanbietern gehostet werden, Daten an andere, unabhängig betriebene und im Besitz von Anderen befindliche Server im Netzwerk gesendet, über die wir keine Kontrolle haben.


<a name="translate"></a>
<br>
## 4.9. - **Disroot Übersetzer** (https://translate.disroot.org)

- Dieser Dienst erfordert keine Anmeldung mit **Disroot**-Zugangsdaten.
- Es werden **keine Log-Daten** (IP-Adresse, Session-Cookie, etc.) auf dem Server gespeichert.
- Es werden keine Benutzerdaten dauerhaft auf dem Server gespeichert.


<a name="calls"></a>
<br>
## 4.10. - **Disroot Calls** (https://calls.disroot.org)

- Für diesen Dienst ist keine Anmeldung erforderlich.
- Es werden **keine Log-Daten** (IP-Adresse, Session-Cookie, etc.) auf dem Server gespeichert.
- Es werden keine Benutzerdaten dauerhaft auf dem Server gespeichert.


<a name="git"></a>
<br>
## 4.11. - **Disroot-Git** (https://git.disroot.org)

- Dieser Dienst erfordert die Erstellung eines separaten Git-Kontos, um mit anderen zu interagieren.
- Die Serverprotokolle, die Informationen wie z. B. Ihre IP-Adresse, Ihren Benutzernamen, Fehlermeldungen und den Benutzeragenten speichern, werden für einen Zeitraum von 24 Stunden gespeichert und danach vom Server gelöscht. Es wird keine Sicherungskopie der Protokolldateien erstellt. Die Protokolle werden aufbewahrt, um Brute-Force-Angriffe auf Konten zu verhindern und um bei der Fehlersuche einen schnellen Einblick zu erhalten.

- Alle Git-Daten, wie z. B. Benutzernamen, E-Mail-Adressen, Nachrichten, Code, Dateien, Versionen, Pull-Requests usw., werden auf dem Server in der Datenbank in unveränderter Form (Klartext) gespeichert.

<a name="audio"></a>
<br>
## 4.12. - **Disroot Audio** (https://mumble.disroot.org)

- Dieser Dienst erfordert eine Verbindung mit einem Benutzernamen, aber es ist nicht zwingend erforderlich, ihn auf dem Server zu registrieren.
- Es werden **keine Log-Daten** (IP-Adresse, Session-Cookie, etc.) auf dem Server gespeichert. sondern nur der Benutzername und das zugehörige Zertifikat, falls der Benutzer sich für eine Registrierung entschieden hat.

<a name="cryptpad"></a>
<br>
## 4.13. - **Disroot CryptPad** (https://cryptpad.disroot.org)

- Für diesen Dienst ist keine Anmeldung oder Angabe von persönlichen Daten erforderlich. Es kann jedoch ein Konto erstellt werden, um Dateien zu speichern.
- Es werden **keine Log-Daten** (IP-Adresse, Session-Cookie, etc.) auf dem Server gespeichert.
- Alle Dokumente, die erstellt oder auf den Server hochgeladen werden, sind Ende-zu-Ende-verschlüsselt, d. h. niemand mit Zugang zum Server kann die Daten entschlüsseln/lesen.
- Die Dokumente verfallen nach drei Monaten und werden dann vom Server entfernt, es sei denn, es wurde ein Konto eingerichtet; in diesem Fall werden die auf den Server hochgeladenen Dokumente entsprechend der vom Nutzer beim Hochladen/Erstellen festgelegten Aufbewahrungsfrist gelöscht.

<a name="akkoma"></a>
<br>
## 4.14. - **Disroot Akkoma** (https://fe.disroot.org)

- Dieser Dienst erfordert eine Anmeldung mit **Disroot**-Zugangsdaten.
- Serverprotokolle, die Informationen wie z.B. Ihre IP-Adresse und Ihren Benutzernamen speichern, werden für einen Zeitraum von 24 Stunden gespeichert und danach vom Server gelöscht. Es wird keine Sicherungskopie der Protokolldateien erstellt. Die Protokolle werden aufbewahrt, um Brute-Force-Angriffe auf Konten zu verhindern und um bei der Fehlersuche einen schnellen Einblick zu erhalten.
- Alle Akkoma-Daten, wie z. B. Nachrichten, Dateien, usw., werden auf dem Server in der Datenbank in unveränderter Form (Klartext) gespeichert.
- Da ActivityPub über ein **föderiertes Protokoll** funktioniert, werden bei der Interaktion mit Benutzern, die auf Servern von Drittanbietern gehostet werden, Daten an andere, unabhängig betriebene und im Besitz von Drittanbietern befindliche Server im Netzwerk gesendet, über die wir keine Kontrolle haben.


<a name="rights"></a>
<br>
# 5. Ihre Rechte

Gemäß der **Datenschutz-Grundverordnung** haben Sie eine Reihe von Rechten in Bezug auf Ihre personenbezogenen Daten:
- **Recht auf Zugang** - Das Recht, (I) Kopien Ihrer personenbezogenen Daten oder (II) jederzeit Zugang zu den von Ihnen übermittelten und bei uns gespeicherten Informationen zu verlangen.
- **Recht auf Berichtigun**g - Das Recht, Ihre Daten berichtigen zu lassen, wenn sie unrichtig oder unvollständig sind.*
- **Recht auf Löschung** - Das Recht, die Löschung oder Entfernung Ihrer Daten von unseren Servern zu verlangen.*
- **Recht auf Einschränkung der Verwendung Ihrer Daten** - Das Recht, die Verarbeitung Ihrer Daten einzuschränken oder die Art und Weise, wie wir Ihre Daten verwenden, zu begrenzen.
- **Recht auf Datenübertragbarkeit** - Das Recht, Ihre Daten zu verschieben, zu kopieren oder zu übertragen.
- **Recht auf Widerspruch** - Das Recht, der Verwendung Ihrer Daten durch uns zu widersprechen.

> *Ihr* **Disroot-Benutzername** und *Ihre* **Disroot-E-Mail-Adresse** sind fester Bestandteil Ihres Benutzerkontos und können nicht geändert werden.*
> Benutzernamen verbleiben in der Datenbank, auch nach einer Löschungsanfrage, um zu verhindern, dass alte Benutzernamen von neuen Benutzern wiederverwendet werden, was die Privatsphäre beider gefährdet und einen möglichen Identitätsdiebstahl ermöglicht. Aus diesem Grund verbleiben Benutzernamen von Konten, die gelöscht wurden, in der Datenbank, um eine Wiederverwendung zu verhindern. Alle damit verbundenen personenbezogenen Daten werden jedoch dauerhaft gelöscht.*

Sie haben das Recht, eine Beschwerde einzureichen, Nachforschungen anzustellen, eines der oben beschriebenen Rechte auszuüben oder Ihre Zustimmung zur Verarbeitung Ihrer Daten zu widerrufen (wenn die Zustimmung unsere Rechtsgrundlage für die Verarbeitung Ihrer Daten ist), indem Sie sich per E-Mail an uns wenden:

- **data.protection.officer@disroot.org** - Verantwortliche Person für diese Datenschutzerklärung
- **info@disroot.org** - Kontakt für allgemeine Informationen

Im Sinne der **GDPR** (**DSGVO**) ist **Disroot.org** der "Datenverantwortliche". Das bedeutet, dass **Disroot.org** bestimmt, zu welchen Zwecken und auf welche Weise Ihre Daten verarbeitet werden.

**Disroot.org-Stiftung**:
Niederländische Handelskammer (KVK) Nummer: 69988099

Wenn Sie mit der Art und Weise, wie Ihre Daten von uns verarbeitet werden, nicht zufrieden sind oder glauben, dass die Verarbeitung nicht angemessen ist, haben Sie das Recht, eine Beschwerde an das **Information Commissioners' Office** zu richten.
Autoriteit Persoonsgegevens PO Box 93374 2509 AJ DEN HAAG Phone: (+31) - (0)70 - 888 85 00 Fax: (+31) - (0)70 - 888 85 01


**Niederländische Datenschutzbehörde (Dutch DPA) **
**Post-Anschrift**  
Autoriteit Persoonsgegevens
PO Box 93374
2509 AJ DEN HAAG
**Telefon:** (+31) - (0)70 - 888 85 00
**Fax:** (+31) - (0)70 - 888 85 01

<a name="access_information"></a>
<br>
## 5.1. Zugang zu Ihren Informationen

Der Zugang zu Ihren persönlichen Daten, gespeicherten Dateien und anderen Informationen, die Sie für einen der von **Disroot.org** angebotenen Dienste zur Verfügung stellen, unterliegt Ihrer Kontrolle. Das bedeutet, dass Sie alle auf unseren Diensten gespeicherten Daten, die an persönliche Informationen gebunden sind (Dienste, die eine Anmeldung erfordern), entweder zu Archivierungszwecken oder zur Übertragung auf einen anderen kompatiblen Dienst herunterladen können.
- [**Hier**](https://howto.disroot.org/en/tutorials/user/gdpr) erfahren Sie, wie Sie auf Ihre persönlichen Daten zugreifen und diese selbst exportieren können
- Ändern Sie Ihre persönlichen Daten oder löschen Sie Ihr Konto [**hier**](https://user.disroot.org)

<a name="changes"></a>
<br>
# 6. Änderungen an dieser Datenschutzerklärung

Alle Änderungen an dieser **Datenschutzerklärung** werden öffentlich zugänglich gemacht und allen Nutzern über unsere sozialen Netzwerke und Blogposts mitgeteilt. Wir empfehlen Ihnen, sich regelmäßig über Änderungen an dieser Erklärung zu informieren.

Sie können den Verlauf der Änderungen an diesem Dokument in unserem Versionskontrollsystem [**hier**](https://git.disroot.org/Disroot/Disroot-Privacy-Policy/commits/branch/main) oder hier im Kapitel 6 des Dokumentes verfolgen.

#### Letzte Aktualisierung dieser Datenschutzerklärung:

- Februar, 2024: Hinzufügen von Informationen zu Translate, Movim und Scribe. Forum und Project Board entfernt.
- April, 2023: Ergänzung von Upload-Informationen über die Speicherdauer von Dateien/Daten.
- Dezember, 2022: Akkoma Informationen hinzugefügt.
- September 2022: Aktualisierung der XMPP-Informationen über die Speicherdauer von Dateien.

<br>
[Back to top](#top)
