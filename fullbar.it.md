---
title: Privacy Statement
bgcolor: '#FFF'
fontcolor: '#1F5C60'
section_id: privacy
text_align: left
---

<br>
**v1.5 - February 2024**

<a name="about"></a>
<br>
## Informazioni su questo documento

Questo documento è stato originariamente scritto in inglese ed è l'unica versione per la quale **Stichting Disroot.org** può essere ritenuta responsabile.<br>
Qualsiasi traduzione di questa **Dichiarazione sulla Privacy** è uno sforzo comunitario per rendere le informazioni accessibili in altre lingue e dovrebbe essere considerata come tale, con un valore puramente informativo.
<br>

<a name="definitions"></a>
<br>
## Definizioni utilizzate in questa Dichiarazione sulla Privacy

- **GDPR**: Regolamento Generale sulla Protezione dei Dati, [UE 2016/679](https://eur-lex.europa.eu/legal-content/EN/TXT/?qid=1580499932731&uri=CELEX:32016R0679)
- **Dati**: Secondo il **GDPR**, i dati sono qualsiasi informazione che può essere utilizzata per identificare una persona, sia direttamente (nome reale, numero di telefono, indirizzo IP, ecc.) sia indirettamente (qualsiasi combinazione dei suddetti più impronte digitali del dispositivo, cookie, ecc.). Nel contesto specifico dell'uso della nostra piattaforma, si tratta delle informazioni minime necessarie per il corretto funzionamento dei servizi forniti da **Disroot.org** nonché delle informazioni che l'utente invia facoltativamente su ciascuno di essi.
- **Servizi**: l'insieme di diversi software, protocolli e standard utilizzati per scambiare dati tra applicazioni web.
- **Utente** o **tu**: qualsiasi persona o terza parte che accede e utilizza i servizi forniti da **Disroot.org**.
- **Disroot, Disroot.org, noi** o **ci**: Stichting Disroot.org
- **Piattaforma**: l'insieme dei servizi forniti da **Disroot.org** e ospitati sui nostri server.
- **Credenziali Disroot**: sono il nome utente e la password creati e utilizzati dall'utente per accedere ai servizi da noi forniti.
- **Servizi federati**: servizi che operano sulla base dei cosiddetti **protocolli di federazione** che consentono agli utenti registrati presso diversi fornitori di servizi di interagire tra loro. Esempi di questi servizi sono **Nextcloud**, **Email**, **Akkoma** e **XMPP**.
- **Attacco brute-force**: è un attacco crittografico che consiste nell'inviare molte password o passphrase, sperando di trovare alla fine quelle corrette.

<br>

<a name="coverage"></a>
<br>
## I Dati coperti da questa Dichiarazione sulla Privacy

Questa **Dichiarazione sulla Privacy** si applica a tutti i servizi ospitati su **Disroot.org** e sui suoi sotto-domini. Non si estende a nessun sito web o servizio web che può essere accessibile dalla nostra piattaforma inclusi, ma non limitati a, qualsiasi servizio federato e siti web di social media al di fuori di **Disroot**. I servizi federati sono quelli che interoperano tra loro (scambiando informazioni e servizi) indipendentemente dal fornitore (ad esempio email o reti sociali aperte). Questi servizi utilizzano protocolli che necessariamente condividono o trasferiscono dati tra diversi fornitori e quindi tali interazioni sono al di fuori dell'ambito di questa Dichiarazione sulla Privacy.<br>
È importante notare che **la condivisione dei dati con altri fornitori di servizi è una scelta dell'utente** (vedi [1. Quali dati raccogliamo?](#data_we_collect)) e viene configurata dagli utenti nelle impostazioni dei loro servizi, inclusa la decisione su cosa condividere e con chi.

<br>

<a name="data_we_collect"></a>
<br>
# 1. Quali dati raccogliamo?

Se un utente sceglie di utilizzare uno qualsiasi dei servizi forniti da noi, i seguenti dati saranno richiesti e quindi raccolti da **Disroot.org**:

- Un indirizzo email valido: richiesto per la creazione dell'account. Questo indirizzo email viene eliminato dal nostro database dopo che l'account è stato approvato/rifiutato, a meno che l'utente scelga, durante il processo di registrazione, di conservarlo per il processo di reimpostazione della password.
- Un nome utente e una password: richiesti per identificare il titolare dell'account e fornire i servizi offerti da **Disroot.org**.
- Informazioni necessarie relative al funzionamento e all'operatività dei servizi che possono includere, ad esempio, indirizzo IP, User Agent, ecc. *Informazioni più dettagliate su questo e su come gestiamo questi dati possono essere trovate nelle [Informative sulla privacy per i servizi](#per_services).*
- Quando un utente effettua una donazione online a **Disroot.org**, raccogliamo dati personali come nome utente (se presente), paese (in caso di richiesta di spazio extra per scopi fiscali), ID transazione o conto bancario/riferimento. Lo scopo per cui utilizziamo questi dati è meramente amministrativo (verifica delle donazioni regolari, gestione contabile) e vengono mantenuti sotto le stesse misure di sicurezza descritte nella sezione "[Come conserviamo i tuoi dati?](#how_we_store)". Poiché tutti i dati che raccogliamo sono precedentemente elaborati da un processore di pagamenti di terze parti come PayPal, Patreon o Liberapay, utilizzando questi o servizi simili, l'uso delle tue informazioni è basato sui loro termini di servizio e politiche, non sui nostri, quindi ti incoraggiamo a rivedere attentamente tali politiche.
- Qualsiasi informazione aggiuntiva che l'utente sceglie di fornire durante l'utilizzo dei servizi forniti da noi (che si tratti di chat, post, email, ecc.). Queste informazioni aggiuntive sono opzionali e con il consenso dell'utente.

(Per informazioni più dettagliate, si prega di fare riferimento alla sezione [Informative dettagliate sulla privacy per i servizi](#per_services) qui sotto)

<a name="what_we_do"></a>
<br>
## 1.1. Cosa facciamo con i tuoi dati?

- Il nostro trattamento delle tue informazioni è limitato alla fornitura del servizio.
- Conserviamo i registri della tua attività per un periodo non superiore a 24 ore (a meno che non sia specificato diversamente per servizio). Questi dati sono utilizzati per aiutare a diagnosticare problemi software, mantenere la sicurezza del sistema contro intrusioni e monitorare lo stato di salute della piattaforma.

(Informazioni dettagliate nella sezione [Informative sulla privacy per i servizi](#per_services))

<a name="how_we_store"></a>
<br>
## 1.2. Come conserviamo i tuoi dati?

Per proteggere i tuoi dati utilizziamo le seguenti misure di sicurezza:

- Utilizziamo la crittografia del disco su tutti i server per prevenire perdite di dati in caso di furto, confisca o manomissione fisica dei server.
- Forniamo e richiediamo la crittografia SSL/TLS su tutte le comunicazioni "utente-a-server" e "server-a-server" su tutti i servizi forniti.
- Utilizziamo tecnologie di crittografia "end-to-end" e/o "server-side" ogni volta che sono disponibili nei servizi che lo consentono, per fornire la massima sicurezza agli utenti.

<a name="how_we_backup"></a>
<br>
## 1.3. Come eseguiamo il backup dei tuoi dati?

Per consentire il tentativo di recupero in situazioni di perdita di dati, creiamo backup su un server remoto di proprietà di Stichting Disroot.org. Eseguiamo il backup dei dati quotidianamente e li conserviamo per un periodo di 4 giorni.

<a name="what_we_do_not"></a>
<br>
# 2. Cosa non facciamo con i tuoi dati

- Non raccogliamo altri dati oltre a quelli necessari per fornirti il servizio.
- Non trattiamo né analizziamo in alcun modo il tuo comportamento o le tue caratteristiche personali per creare profili su di te o sul tuo utilizzo dei servizi. Non abbiamo pubblicità né rapporti commerciali con inserzionisti.
- Non vendiamo i tuoi dati a nessuna terza parte.
- Non condividiamo i tuoi dati con nessuna terza parte, salvo nel caso di servizi federati che richiedono la condivisione di determinati dati per poter operare (ad esempio, un altro fornitore di servizi email deve conoscere il tuo indirizzo email per poter consegnare le email).
- Non richiediamo alcuna informazione aggiuntiva che non sia cruciale per il funzionamento del servizio (non chiediamo numeri di telefono, dati personali privati, indirizzi di casa).
- Non leggiamo/osserviamo né trattiamo i tuoi dati personali, email, file, ecc., memorizzati sui nostri server a meno che non sia necessario per fornire il servizio, per scopi di risoluzione dei problemi o in caso di sospetto di violazione dei nostri **Termini di Servizio**, nel qual caso chiediamo il tuo permesso preventivo o ti informiamo successivamente di tutte le azioni intraprese contro l'account nel rapporto di trasparenza indirizzato al titolare dell'account.

<a name="where_store"></a>
<br>
# 3. Dove sono conservati i dati?

Conserviamo tutti i dati nei **nostri server**, situati in un data center nei **Paesi Bassi**.

<a name="per_service"></a>
<br>
# 4. Informative dettagliate sulla privacy per servizio

<a name="email"></a>
<br>
## 4.1. - **Email Disroot** (https://webmail.disroot.org)

- Questo servizio richiede l'accesso con le credenziali di **Disroot**.
- Tutte le email, a meno che non siano crittografate dall'utente (con GnuPG/PGP, ad esempio), sono memorizzate in chiaro sui nostri server.
- Gli indirizzi IP degli utenti attualmente connessi tramite i protocolli IMAP/POP3 sono memorizzati finché il dispositivo è connesso al server *(per ciascun dispositivo connesso)*.
- I log del server, che memorizzano informazioni come, ma non limitate a, il tuo nome utente e il tuo indirizzo IP, gli indirizzi email *da* e *a*, gli indirizzi IP dei server da cui provengono o verso cui vanno le email, sono conservati per un periodo di 24 ore dopo di che vengono eliminati dal server. Non viene creato alcun backup dei file di log. I log sono conservati per prevenire attacchi brute-force sugli account e per fornire una rapida visione quando si risolvono problemi.
- Dato che l'email funziona su un protocollo **federato**, quando interagisci con indirizzi email ospitati su server di terze parti (ad esempio Gmail.com, Posteo.org), i dati vengono inviati ad altri server della rete gestiti e di proprietà indipendente sui quali non abbiamo alcun controllo.

<a name="cloud"></a>
<br>
## 4.2. - **Cloud Disroot** (https://cloud.disroot.org)

- Questo servizio richiede l'accesso con le credenziali di **Disroot**.
- Tutti i file inviati al cloud sono crittografati con una coppia di chiavi creata in base alla password dell'utente per aggiungere un ulteriore livello di sicurezza. Nota, tuttavia, che le chiavi sono memorizzate sul server, il che compromette il livello di sicurezza in una certa misura (ad esempio: se un attaccante conosce la tua password e ottiene la coppia di chiavi di crittografia, può decrittografare i dati). Tuttavia, **non** esiste alcuna "Master Key" nella nostra configurazione, il che significa che gli amministratori non possono decrittografare nessun file memorizzato nel cloud senza conoscere preventivamente la password dell'utente.
- Escludendo i file, tutto il resto (calendari, contatti, notizie, attività, segnalibri, ecc.) è memorizzato in chiaro in un database, a meno che un'applicazione non fornisca crittografia esterna (nessuna finora). Questa è una limitazione del software che utilizziamo per questo servizio (Nextcloud).
- I log del server, che memorizzano informazioni come, ma non limitate a, il tuo indirizzo IP, il tuo nome utente, un'app attualmente utilizzata, messaggi di errore e User Agent, sono conservati per un periodo di 24 ore dopo di che vengono eliminati dal server. Non viene creato alcun backup dei file di log. I log sono conservati per prevenire attacchi brute-force sugli account e per fornire una rapida visione quando si risolvono problemi.

<a name="chat"></a>
<br>
## 4.3. - **Chat XMPP Disroot** (https://webchat.disroot.org)

- Questo servizio richiede l'accesso con le credenziali di **Disroot**.
- La rubrica (la tua lista di contatti XMPP) è memorizzata nel database del server.
- La cronologia delle chat è memorizzata sul server nella stessa forma in cui si trova nella chat stessa, il che significa che le chat non crittografate sono memorizzate in testo semplice e le chat crittografate sono memorizzate crittografate. Inoltre, la cronologia delle chat, se non specificato dall'utente per ogni chatroom, è memorizzata sul server per un periodo di 1 mese. Puoi decidere di non memorizzare alcuna cronologia sul server per ogni chat.
- I log del server, che memorizzano informazioni come, ma non limitate a, il tuo indirizzo IP e il tuo nome utente, sono conservati per un periodo di 24 ore dopo di che vengono eliminati dal server. Non viene creato alcun backup dei file di log. I log sono conservati per prevenire attacchi brute-force sugli account e per fornire una rapida visione quando si risolvono problemi.
- Dato che XMPP è un protocollo **federato**, quando interagisci con utenti o chatroom ospitate su server di terze parti, i dati vengono inviati ad altri server della rete gestiti e di proprietà indipendente sui quali non abbiamo alcun controllo.
- I file caricati sul server sono memorizzati così come sono (testo semplice o crittografato) per un periodo di 1 mese.
- Se utilizzi [Movim](https://webchat.disroot.org), tutti gli articoli e i commenti pubblicati da te sui nostri servizi sono memorizzati sul server. Utilizzando il pannello di configurazione di Movim puoi scegliere di lasciare l'istanza. Se lo fai, tutti i dati relativi al tuo account XMPP saranno distrutti.

<a name="search"></a>
<br>
## 4.4. - **Disroot Search** (https://search.disroot.org)

- Questo servizio non richiede l'accesso o la fornitura di dati personali.
- **Nessun dato di log** (indirizzo IP, cookie di sessione, ecc.) è memorizzato sul server.
- I log possono essere abilitati occasionalmente in caso di risoluzione dei problemi. I log sono quindi abilitati per la durata della valutazione del problema e vengono eliminati immediatamente dopo.
- Le informazioni personali dei nostri utenti non vengono mai divulgate ad altri motori di ricerca.

<a name="upload"></a>
<br>
## 4.5. - **Upload Disroot** (https://upload.disroot.org)

- Questo servizio non richiede l'accesso o la fornitura di dati personali.
- **Nessun dato di log** (indirizzo IP, cookie di sessione, ecc.) è memorizzato sul server.
- I log possono essere abilitati occasionalmente in caso di risoluzione dei problemi. I log sono quindi abilitati per la durata della valutazione del problema e vengono eliminati immediatamente dopo.
- Tutti i file caricati sul server sono **crittografati end-to-end**, il che significa che nessuno con accesso al server può decrittare/leggere i dati.
- I file caricati sul server vengono cancellati in base al periodo di conservazione impostato dall'utente al momento del caricamento. Tuttavia, sono previsti periodi di conservazione massimi automaticamente impostati in base alla dimensione del file (più grande è il file, più breve è il tempo in cui viene mantenuto sul server).

<a name="pads"></a>
<br>
## 4.6. - **Pads Disroot** (https://pad.disroot.org)

- Questo servizio non richiede l'accesso o la fornitura di dati personali.
- **Nessun dato di log** (indirizzo IP, cookie di sessione, ecc.) è memorizzato sul server.
- Non raccogliamo altri dati personali che possono essere collegati ai pads.
- Il contenuto del pad è memorizzato sul server nel database così com'è (testo semplice).
- Gli utenti non possono eliminare i pads, ma i pads non toccati scadono dopo sei mesi e vengono quindi rimossi dal server.

<a name="bin"></a>
<br>
## 4.7. - **Bin Disroot** (https://bin.disroot.org)

- Questo servizio non richiede l'accesso o la fornitura di dati personali.
- **Nessun dato di log** (indirizzo IP, cookie di sessione, ecc.) è memorizzato sul server.
- I log possono essere abilitati occasionalmente in caso di risoluzione dei problemi. I log sono quindi abilitati per la durata della valutazione del problema e vengono eliminati immediatamente dopo.
- Tutti i file caricati sul server sono **crittografati end-to-end**, il che significa che nessuno con accesso al server può decrittare/leggere i dati.
- I file caricati sul server vengono cancellati in base al periodo di conservazione impostato dall'utente al momento del caricamento.
- I commenti e le discussioni sotto i paste sono **crittografati end-to-end**.

<a name="scribe"></a>
<br>
## 4.8. - **Scribe Disroot** (https://scribe.disroot.org)

- Questo servizio richiede di creare un account separato per interagire con le discussioni.
- I log del server, che memorizzano informazioni come, ma non limitate a, il tuo indirizzo IP e il tuo nome utente, sono conservati per un periodo di 24 ore dopo di che vengono eliminati dal server. Non viene creato alcun backup dei file di log. I log sono conservati per prevenire attacchi brute-force sugli account e per fornire una rapida visione quando si risolvono problemi.
- Tutti i dati di Scribe come, ma non limitati a, comunità, messaggi, file, ecc., sono memorizzati sul server nel database così com'è (testo semplice).
- Dato che ActivityPub è un protocollo **federato**, quando interagisci con utenti ospitati su server di terze parti, i dati vengono inviati ad altri server della rete gestiti e di proprietà indipendente sui quali non abbiamo alcun controllo.

<a name="translate"></a>
<br>
## 4.9. - **Traduttore Disroot** (https://translate.disroot.org)

- Questo servizio non richiede l'accesso.
- **Nessun dato di log** (indirizzo IP, cookie di sessione, ecc.) è memorizzato sul server.
- Nessun dato dell'utente è memorizzato permanentemente sul server.


<a name="calls"></a>
<br>
## 4.10. - **Videochiamate Disroot** (https://calls.disroot.org)

- Questo servizio non richiede l'accesso.
- **Nessun dato di log** (indirizzo IP, cookie di sessione, ecc.) è memorizzato sul server.
- Nessun dato dell'utente è memorizzato permanentemente sul server.


<a name="git"></a>
<br>
## 4.11. - **Git Disroot** (https://git.disroot.org)

- Per interagire con gli altri, è necessario creare un account git separato.
- I log del server, che memorizzano informazioni come, ma non limitate a, il tuo indirizzo IP, il tuo nome utente, messaggi di errore e User Agent, sono conservati per un periodo di 24 ore dopo di che vengono eliminati dal server. Non viene creato alcun backup dei file di log. I log sono conservati per prevenire attacchi brute-force sugli account e per fornire una rapida visione quando si risolvono problemi.
- Tutti i dati di git come, ma non limitati a, nomi utente, indirizzi email, messaggi, codice, file, versioni, richieste di pull, ecc., sono memorizzati sul server nel database così com'è (testo semplice).

<a name="audio"></a>
<br>
## 4.12. - **Audio Disroot** (https://mumble.disroot.org)

- Per questo servizio è necessario connettersi con un nome utente, ma non è obbligatorio registrarlo sul server.
- **Nessun dato di log** (indirizzo IP, cookie di sessione, ecc.) è memorizzato sul server, viene salvato solo il nome utente e il suo certificato associato, se l'utente decide di registrarlo.

<a name="cryptpad"></a>
<br>
## 4.13. - **CryptPad Disroot** (https://cryptpad.disroot.org)

- Questo servizio non richiede l'accesso o la fornitura di dati personali. Tuttavia, è possibile creare un account per salvare i file.
- **Nessun dato di log** (indirizzo IP, cookie di sessione, ecc.) è memorizzato sul server.
- Tutti i documenti creati o caricati sul server sono **crittografati end-to-end**, il che significa che nessuno con accesso al server può decrittare/leggere i dati.
- I documenti scadono dopo tre mesi e vengono quindi rimossi dal server, tranne se è stato creato un account, in tal caso i documenti caricati sul server vengono cancellati in base al periodo di conservazione impostato dall'utente al momento del caricamento/creazione.

<a name="akkoma"></a>
<br>
## 4.14. - **Akkoma Disroot** (https://fe.disroot.org)

- Questo servizio richiede l'accesso con le credenziali di **Disroot**.
- I log del server, che memorizzano informazioni come, ma non limitate a, il tuo indirizzo IP e il tuo nome utente, sono conservati per un periodo di 24 ore dopo di che vengono eliminati dal server. Non viene creato alcun backup dei file di log. I log sono conservati per prevenire attacchi brute-force sugli account e per fornire una rapida visione quando si risolvono problemi.
- Tutti i dati di Akkoma come, ma non limitati a, messaggi, file, ecc., sono memorizzati sul server nel database così com'è (testo semplice).
- Dato che ActivityPub è un protocollo **federato**, quando interagisci con utenti ospitati su server di terze parti, i dati vengono inviati ad altri server della rete gestiti e di proprietà indipendente sui quali non abbiamo alcun controllo.

<a name="rights"></a>
<br>
# 5. I tuoi diritti

In base al **GDPR** hai una serie di diritti riguardo ai tuoi dati personali:

- **Diritto di accesso** - Il diritto di richiedere (I) copie dei tuoi dati personali o (II) accesso alle informazioni che hai inviato e che deteniamo in qualsiasi momento.
- **Diritto di rettifica** - Il diritto di far rettificare i tuoi dati se sono inesatti o incompleti.
- **Diritto alla cancellazione** - Il diritto di richiedere la cancellazione o la rimozione dei tuoi dati dai nostri server.
- **Diritto di limitare l'uso dei tuoi dati** - Il diritto di limitare il trattamento o di limitare il modo in cui usiamo i tuoi dati.
- **Diritto alla portabilità dei dati** - Il diritto di spostare, copiare o trasferire i tuoi dati.
- **Diritto di opposizione** - Il diritto di opporsi al nostro utilizzo dei tuoi dati.

> *Il tuo* **username Disroot** *e* **indirizzo email Disroot** *sono parte integrante del tuo account utente e non possono essere modificati.*
> *I nomi utente rimangono nel database, anche dopo la richiesta di cancellazione, per evitare che i vecchi nomi utente vengano riutilizzati da nuovi utenti, compromettendo la privacy di entrambi e consentendo possibili furti di identità. Per questo motivo, i nomi utente degli account eliminati rimangono nel database per evitare che vengano riutilizzati. Tuttavia, tutte le informazioni personali collegate vengono eliminate permanentemente.*

Hai il diritto di presentare un reclamo, fare domande, esercitare uno qualsiasi dei diritti descritti sopra o revocare il tuo consenso al trattamento dei tuoi dati (dove il consenso è la nostra base giuridica per il trattamento dei tuoi dati), contattandoci via email a:

- **data.protection.officer@disroot.org** - Persona responsabile di questa Informativa sulla privacy
- **info@disroot.org** - Contatto per informazioni generali

Ai fini del **GDPR**, **Disroot.org** è il "titolare del trattamento dei dati". Ciò significa che **Disroot** determina le finalità e le modalità con cui i tuoi dati vengono trattati.

**Stichting Disroot.org**:
Numero di Camera di Commercio olandese (KVK): 69988099

Se non sei soddisfatto del modo in cui i tuoi dati vengono gestiti da noi, o pensi che il loro trattamento non sia appropriato, hai il diritto di inviare un reclamo all'**Ufficio del Commissario per l'Informazione**.

**Autorità olandese per la protezione dei dati (Autoriteit Persoonsgegevens - Dutch DPA)**
**Indirizzo postale**
Autoriteit Persoonsgegevens
PO Box 93374
2509 AJ DEN HAAG
**Telefono:** (+31) - (0)70 - 888 85 00
**Fax:** (+31) - (0)70 - 888 85 01

<a name="access_information"></a>
<br>
## 5.1. Accesso alle tue informazioni

L'accesso ai tuoi dati personali, ai file memorizzati e ad altre informazioni che fornisci a uno qualsiasi dei servizi offerti da **Disroot.org** è sotto il tuo controllo. Ciò significa che tutti i dati memorizzati sui nostri servizi legati alle informazioni personali (servizi che richiedono l'accesso) sono disponibili per te per il download, sia per scopi di archiviazione che per il trasferimento a un altro servizio compatibile.

- Scopri come accedere ed esportare autonomamente i tuoi dati personali [**qui**](https://howto.disroot.org/en/tutorials/user/gdpr)
- Modifica i tuoi dati personali o elimina il tuo account [**qui**](https://user.disroot.org)

<a name="changes"></a>
<br>
# 6. Modifiche a questa Informativa sulla privacy

Tutte le modifiche a questa **Informativa sulla privacy** saranno disponibili pubblicamente e comunicate a tutti gli utenti tramite i nostri social network e post sul blog. Ti raccomandiamo di controllare regolarmente eventuali modifiche a questa Informativa.

Puoi seguire la cronologia delle modifiche a questo documento sul nostro sistema di controllo delle versioni [**qui**](https://git.disroot.org/Disroot/Disroot-Privacy-Policy/commits/branch/main) o nella sezione 6 di questo documento.


#### Ultimi aggiornamenti di questa Informativa sulla privacy:

- Febbraio, 2024: aggiunte informazioni su Translate, Movim e Scribe, rimossi Forum e Project Board, aggiornate le informazioni sui periodi di conservazione del backup.
- Aprile, 2023: aggiunte informazioni su Upload sui periodi di conservazione.
- Dicembre, 2022: aggiunte informazioni su Akkoma.
- Settembre, 2022: aggiornate le informazioni su XMPP sui periodi di conservazione dei file.

<br>
[Back to top](#top)